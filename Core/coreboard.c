/**
 * \file coreboard.c
 * \brief This file contains the core functionalities of the board.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Sun 11:50 PM
 * \copyright
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "coreboard.h"

static MotionMask default_circle_mask(void)
{
	static MotionMask defaultMask = {{
		{1, 0, 1, 0, 1},
		{0, 1, 1, 1, 0},
		{1, 1, 0, 1, 1},
		{0, 1, 1, 1, 0},
		{1, 0, 1, 0, 1}
	}};
	return defaultMask;
}

static MotionMask default_diamond_mask(void)
{
	static MotionMask defaultMask = {{
		{1, 0, 0, 0, 1},
		{0, 1, 0, 1, 0},
		{0, 0, 0, 0, 0},
		{0, 1, 0, 1, 0},
		{1, 0, 0, 0, 1}
	}};
	return defaultMask;
}

static MotionMask default_triangle_mask(void)
{
	static MotionMask defaultMask = {{
		{1, 0, 0, 0, 1},
		{0, 1, 0, 1, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0}
	}};
	return defaultMask;
}

static MotionMask default_square_mask(void)
{
	static MotionMask defaultMask = {{
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
		{1, 1, 0, 1, 1},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0}
	}};
	return defaultMask;
}

MotionMask default_mask_for(const PieceIndex index)
{
	MotionMask (* defaultMask[4])(void);
	defaultMask[0] = default_square_mask;
	defaultMask[1] = default_triangle_mask;
	defaultMask[2] = default_diamond_mask;
	defaultMask[3] = default_circle_mask;
	return defaultMask[index]();
}

Cell * Cell_allocinit_empty(void)
{
	Cell * cell;
	cell = malloc(sizeof(Cell));
	if (cell == NULL) {
		fprintf(stderr, "error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	cell->index = EMPTY;
	cell->owner = NOBODY;
	return cell;
}

static void Cell_set_position(Cell * const cell, const size_t y, const size_t x)
{
	cell->position.y = y;
	cell->position.x = x;
}

void Cell_set(Cell * const cell, const PieceIndex index, const Players owner, const size_t y, const size_t x)
{
	cell->index = index;
	cell->owner = owner;
	Cell_set_position(cell, y, x);
}

static void Cell_free(Cell * * const cell)
{
	free(*cell);
	*cell = NULL;
}

static CoreBoard * CoreBoard_allocinit_empty(void)
{
	CoreBoard * board;
	size_t i, j;
	board = malloc(sizeof(CoreBoard));
	if (board == NULL) {
		fprintf(stderr, "error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			board->board[i][j] = Cell_allocinit_empty();
		}
	}
	return board;
}

VECTOR_FREE(Cell *, Cell)

void CoreBoard_free(CoreBoard * * const board, const NumberOfPlayer n)
{
	CoreBoard * tmp;
	size_t i, j;
	tmp = *board;
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			Cell_free(&(tmp->board[i][j]));
		}
	}
	for (i = 0; i < n; i++) {
		VectorCell_free(&(tmp->tracker[i]));
	}
	free(tmp);
	*board = NULL;
}

static void CoreBoard_init_sides(CoreBoard * const board, const NumberOfPlayer n)
{
	size_t i, k, j;
	for (i = 0; i < 4; i++) {
		j = i + 1;
		k = (BOARD_SQUARE_WIDTH - 2) - i;
		Cell_set(board->board[0][j], i, PLAYER_0, 0, j);
		Cell_set(board->board[0][k], i, PLAYER_0, 0, k);
		Cell_set(board->board[9][j], i, PLAYER_1, 9, j);
		Cell_set(board->board[9][k], i, PLAYER_1, 9, k);
	}
	if (n >= THREE_PLAYER) {
		for (i = 0; i < 4; i++) {
			j = i + 1;
			k = (BOARD_SQUARE_WIDTH - 2) - i;
			Cell_set(board->board[j][0], i, PLAYER_2, j, 0);
			Cell_set(board->board[k][0], i, PLAYER_2, k, 0);
		}
		if (n == FOUR_PLAYER) {
			for (i = 0; i < 4; i++) {
				j = i + 1;
				k = (BOARD_SQUARE_WIDTH - 2) - i;
				Cell_set(board->board[j][9], i, PLAYER_3, j, 9);
				Cell_set(board->board[k][9], i, PLAYER_3, k, 9);
			}
		}
	}
}

static Coordinate Coordinate_diff(const Coordinate c0, const Coordinate c1)
{
	Coordinate result;
	result.y = c0.y - c1.y;
	result.x = c0.x - c1.x;
	return result;
}

static bool Coordinate_is_reachable(const Coordinate what, const Coordinate where)
{
	Coordinate diff;
	diff = Coordinate_diff(where, what);
	return (abs(diff.y) <= 2) && (abs(diff.x) <= 2);
}

bool Coordinate_is_at_border_maskwise(const Coordinate c)
{
	return ((c.y % 4) == 0) || ((c.x % 4) == 0);
}

static bool Coordinate_is_at_border_boardwise(const Coordinate c)
{
	return ((c.y % (BOARD_SQUARE_HEIGHT - 1)) == 0) || ((c.x % (BOARD_SQUARE_WIDTH - 1)) == 0);
}

bool Coordinate_is_in_player_zone(const Coordinate c, const size_t p)
{
	bool result;
	if (p < 2) {
		result = (c.y == (((p + 1) % 2) * 9)) && ((c.x >= 1) && (c.x < BOARD_SQUARE_WIDTH - 1));
	} else {
		result = (c.x == (((p + 1) % 2) * 9)) && ((c.y >= 1) && (c.y < BOARD_SQUARE_HEIGHT - 1));
	}
	return result;
}

static Coordinate Coordinate_from_boardwise_to_maskwise(const Coordinate center, const Coordinate c)
{
	Coordinate result;
	result.y = (c.y - center.y) + 2;
	result.x = (c.x - center.x) + 2;
	return result;
}

Coordinate Coordinate_from_maskwise_to_boardwise(const Coordinate center, const Coordinate c)
{
	Coordinate result;
	result.y = (c.y + center.y) - 2;
	result.x = (c.x + center.x) - 2;
	return result;
}

static bool Coordinate_is_within_coreboard(const Coordinate c)
{
	return (c.y >= 0) && (c.x >= 0) && (c.y < BOARD_SQUARE_HEIGHT) && (c.x < BOARD_SQUARE_WIDTH);
}

static Coordinate Coordinate_middle_point(const Coordinate c0, const Coordinate c1)
{
	Coordinate result;
	result.y = (c0.y + c1.y) / 2;
	result.x = (c0.x + c1.x) / 2;
	return result;
}

static bool MotionMask_can_move(const MotionMask mask, const size_t step)
{
	bool canMoveAtBorder;
	size_t i, j;
	canMoveAtBorder = false;
	i = 0;
	while ((i < 5) && (! canMoveAtBorder)) {
		j = 0;
		while ((j < 5) && (! canMoveAtBorder)) {
			canMoveAtBorder = mask.motion[i][j];
			j += step;
		}
		i += step;
	}
	return canMoveAtBorder;
}

static void MotionMask_disallow_if_at_border(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t who, const size_t i, const size_t j)
{
	Coordinate boardWise, maskWise;
	if (mask->motion[i][j]) {
		maskWise.y = i;
		maskWise.x = j;
		boardWise = Coordinate_from_maskwise_to_boardwise(what, maskWise);
		mask->motion[i][j] = (Coordinate_is_in_player_zone(boardWise, who)) || (! Coordinate_is_at_border_boardwise(boardWise));
	}
}

static void MotionMask_disallow_if_unavailable(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t i, const size_t j)
{
	Coordinate boardWise, maskWise;
	maskWise.y = i;
	maskWise.x = j;
	boardWise = Coordinate_from_maskwise_to_boardwise(what, maskWise);
	if ((mask->motion[i][j] = Coordinate_is_within_coreboard(boardWise))) {
		mask->motion[i][j] = (board->board[boardWise.y][boardWise.x]->index == EMPTY);
	}
}

static void MotionMask_disallow_if_middle_available(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t i, const size_t j)
{
	Coordinate middlePoint, boardWise, maskWise;
	if (mask->motion[i][j]) {
		maskWise.y = i;
		maskWise.x = j;
		boardWise = Coordinate_from_maskwise_to_boardwise(what, maskWise);
		middlePoint = Coordinate_middle_point(what, boardWise);
		mask->motion[i][j] = (board->board[middlePoint.y][middlePoint.x]->index != EMPTY);
	}
}

LIST_MALLOC(Coordinate, Coordinate)
LIST_PUSH_FRONT(Coordinate, Coordinate)
LIST_AT_HEAD(Coordinate, Coordinate)
LIST_AT_TAIL(Coordinate, Coordinate)
LISTELEMENT_ISTAILSENTINEL(Coordinate, Coordinate)
LIST_WHEREIS(Coordinate, Coordinate)
LISTELEMENT_GET(Coordinate, Coordinate)
LIST_FREE(Coordinate, Coordinate)

static bool MotionMask_is_termination_valid(ListCoordinate * const previousPositions, CoreBoard * const board, const PieceIndex index, const size_t who, const Coordinate currentPosition)
{
	bool noValidTermination, result;
	ListCoordinate * borderTerminations;
	ListElementCoordinate * current;
	MotionMask currentMask;
	Coordinate original, maskWise, boardWise;
	size_t i, j;
	currentMask = default_mask_for(index);
	noValidTermination = true;
	borderTerminations = ListCoordinate_malloc();
	i = 0;
	while ((i < 5) && noValidTermination) {
		j = 0;
		while ((j < 5) && noValidTermination) {
			if (currentMask.motion[i][j]) {
				MotionMask_disallow_if_unavailable(&currentMask, board, currentPosition, i, j);
				MotionMask_disallow_if_middle_available(&currentMask, board, currentPosition, i, j);
				if (currentMask.motion[i][j]) {
					maskWise.y = i;
					maskWise.x = j;
					boardWise = Coordinate_from_maskwise_to_boardwise(currentPosition, maskWise);
					currentMask.motion[i][j] = ListElementCoordinate_istailsentinel(ListCoordinate_whereis(previousPositions, boardWise));
					if (currentMask.motion[i][j]) {
						noValidTermination = (! Coordinate_is_in_player_zone(boardWise, who)) && Coordinate_is_at_border_boardwise(boardWise);
						ListCoordinate_push_front(borderTerminations, boardWise);
					}
				}
			}
			j += 2;
		}
		i += 2;
	}
	if (noValidTermination && borderTerminations->size > 0) {
		current = ListCoordinate_at_head(borderTerminations);
		ListCoordinate_push_front(previousPositions, currentPosition);
		while ((! ListElementCoordinate_istailsentinel(current)) && noValidTermination) {
			noValidTermination = (! MotionMask_is_termination_valid(previousPositions, board, index, who, ListElementCoordinate_get(current)));
			current = current->next;
		}
	}
	ListCoordinate_free(&borderTerminations);
	result = (! noValidTermination);
	return result;
}


static void MotionMask_disallow_if_terminate_in_border(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const PieceIndex index, const size_t who, const size_t i, const size_t j)
{
	ListCoordinate * previousPositions;
	Coordinate boardWise, maskWise;
	if (mask->motion[i][j]) {
		maskWise.y = i;
		maskWise.x = j;
		boardWise = Coordinate_from_maskwise_to_boardwise(what, maskWise);
		previousPositions = ListCoordinate_malloc();
		if ((! Coordinate_is_at_border_boardwise(boardWise)) || (Coordinate_is_in_player_zone(boardWise, who))) {
			boardWise = what;
		} else {
			ListCoordinate_push_front(previousPositions, what);
		}
		mask->motion[i][j] = MotionMask_is_termination_valid(previousPositions, board, index, who, boardWise);
		ListCoordinate_free(&previousPositions);
	}
}


void MotionMask_filter(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t who, const PieceIndex index)
{
	size_t i, j;
	for (i = 1; i < 4; i++) {
		for (j = 1; j < 4; j++) {
			if (mask->motion[i][j]) {
				MotionMask_disallow_if_unavailable(mask, board, what, i, j);
				MotionMask_disallow_if_at_border(mask, board, what, who, i, j);
			}
		}
	}
	for (i = 0; i < 5; i += 2) {
		for (j = 0; j < 5; j += 2) {
			if (mask->motion[i][j]) {
				MotionMask_disallow_if_unavailable(mask, board, what, i, j);
				MotionMask_disallow_if_middle_available(mask, board, what, i, j);
				MotionMask_disallow_if_terminate_in_border(mask, board, what, index, who, i, j);
			}
		}
	}
}

bool CoreBoard_is_owner(CoreBoard * const board, const Players who, const Coordinate what)
{
	return board->board[what.y][what.x]->owner == who;
}

bool CoreBoard_can_jump(CoreBoard * const board, const size_t who, const Coordinate what, ListCoordinate * const previousPositions)
{
	bool canJump;
	size_t i, j;
	PieceIndex index;
	Coordinate maskWise, boardWise;
	MotionMask filteredMask;
	canJump = false;
	index = board->board[what.y][what.x]->index;
	filteredMask = default_mask_for(index);
	MotionMask_filter(&filteredMask, board, what, who, index);
	for (i = 0; i < 5; i++) {
		for (j = 0; j < 5; j++) {
			if (filteredMask.motion[i][j]) {
				maskWise.y = i;
				maskWise.x = j;
				boardWise = Coordinate_from_maskwise_to_boardwise(what, maskWise);
				filteredMask.motion[i][j] = ListElementCoordinate_istailsentinel(ListCoordinate_whereis(previousPositions, boardWise));
			}
		}
	}
	canJump = MotionMask_can_move(filteredMask, 2);
	return canJump;
}

void CoreBoard_alter_board(CoreBoard * const board, const Coordinate what, const Coordinate where)
{
	Cell * tmp;
	tmp = board->board[what.y][what.x];
	Cell_set_position(tmp, where.y, where.x);
	board->board[what.y][what.x] = board->board[where.y][where.x];
	board->board[where.y][where.x] = tmp;
}

Displacement CoreBoard_move(CoreBoard * const board, const size_t who, const Coordinate what, const Coordinate where)
{
	Displacement disp;
	MotionMask filteredMask;
	Coordinate whereMaskWise;
	PieceIndex index;
	disp = STATIONARY;
	if (CoreBoard_is_owner(board, Players_symbol_for(who), what)) {
		if (Coordinate_is_reachable(what, where)) {
			index = board->board[what.y][what.x]->index;
			filteredMask = default_mask_for(index);
			MotionMask_filter(&filteredMask, board, what, who, index);
			whereMaskWise = Coordinate_from_boardwise_to_maskwise(what, where);
			if (filteredMask.motion[whereMaskWise.y][whereMaskWise.x]) {
				disp = WALKED;
				if (Coordinate_is_at_border_maskwise(whereMaskWise)) {
					disp = JUMPED;
				}
				CoreBoard_alter_board(board, what, where);
			}
		}
	}
	return disp;
}

bool CoreBoard_did_player_win(CoreBoard * const board, const size_t player)
{
	size_t i, c;
	i = 1;
	c = ((player + 1) % 2) * 9;
	if ((player < 2)) {
		while ((i < BOARD_SQUARE_WIDTH - 1) && (board->board[c][i]->owner == Players_symbol_for(player))) {
			i++;
		}
	} else {
		while ((i < BOARD_SQUARE_HEIGHT - 1) && (board->board[i][c]->owner == Players_symbol_for(player))) {
			i++;
		}
	}
	return i == (BOARD_SQUARE_HEIGHT - 1);
}

VECTOR_GET(Cell *, Cell)

bool CoreBoard_can_skip(CoreBoard * const board, const size_t who)
{
	Cell * currentPlayerPiece;
	MotionMask currentMask;
	PieceIndex index;
	size_t i;
	bool canSkip;
	canSkip = false;
	i = 0;
	do {
		currentPlayerPiece = VectorCell_get(board->tracker[who], i);
		index = currentPlayerPiece->index;
		currentMask = default_mask_for(index);
		MotionMask_filter(&currentMask, board, currentPlayerPiece->position, who, index);
		canSkip = (! MotionMask_can_move(currentMask, 1));
		i++;
	} while ((i < 8) && (! canSkip));
	return canSkip;
}

VECTOR_MALLOC(Cell *, Cell)
VECTOR_PUSH_BACK(Cell *, Cell)

static void CoreBoard_init_tracker(CoreBoard * const board, const NumberOfPlayer n)
{
	size_t i;
	board->tracker[0] = VectorCell_malloc(8);
	board->tracker[1] = VectorCell_malloc(8);
	for (i = 1; i < BOARD_SQUARE_WIDTH - 1; i++) {
		VectorCell_push_back(board->tracker[0], board->board[0][i]);
		VectorCell_push_back(board->tracker[1], board->board[9][i]);
	}
	if (n >= THREE_PLAYER) {
		board->tracker[2] = VectorCell_malloc(8);
		for (i = 1; i < BOARD_SQUARE_HEIGHT - 1; i++) {
			VectorCell_push_back(board->tracker[2], board->board[i][0]);
		}
		if (n == FOUR_PLAYER) {
			board->tracker[3] = VectorCell_malloc(8);
			for (i = 1; i < BOARD_SQUARE_HEIGHT - 1; i++) {
				VectorCell_push_back(board->tracker[3], board->board[i][9]);
			}
		}
	}
}

bool CoreBoard_is_any_piece_unmoved(CoreBoard * const board, const NumberOfPlayer n)
{
	size_t i, j;
	bool isUnmoved;
	VectorCell * currentPlayerCells;
	isUnmoved = false;
	i = 0;
	do {
		currentPlayerCells = board->tracker[i];
		j = 0;
		do {
			isUnmoved = Coordinate_is_in_player_zone(VectorCell_get(currentPlayerCells, j)->position, i + 1);
			j++;
		} while ((j < 8) && (! isUnmoved));
		i++;
	} while ((i < n) && (! isUnmoved));
	return isUnmoved;
}

CoreBoard * CoreBoard_generate(const StartupSettings settings, size_t * const currentPlayerIndex, size_t * const roundCounter)
{
	CoreBoard * board;
	board = CoreBoard_allocinit_empty();
	CoreBoard_init_sides(board, settings.n);
	CoreBoard_init_tracker(board, settings.n);
	srand(time(NULL));
	*currentPlayerIndex = rand() % settings.n;
	*roundCounter = 0;
	return board;
}

void disp(CoreBoard * b){
	size_t i, j;
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			printf("%c ", b->board[i][j]->owner);
		}
		putchar('\n');
	}
	puts("                            ");
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			switch (b->board[i][j]->index) {
				case SQUARE:
					printf("+ ");
					break;
				case TRIANGLE:
					printf("Y ");
					break;
				case CIRCLE:
					printf("* ");
					break;
				case DIAMOND:
					printf("X ");
					break;
				case EMPTY:
					printf(". ");
					break;
				default:
					break;
			}
		}
		putchar('\n');
	}
	puts("=============");
}

static void MotionMask_disp(MotionMask mask) {
	size_t i, j;
	for (i = 0; i < 5; i++) {
		for (j = 0; j < 5; j++) {
			printf("%d ", mask.motion[i][j]);
		}
		putchar('\n');
	}
}
