/**
 * \file coreboard.h
 * \brief This file contains the core functionalities of the board.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Sun 11:50 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __CORE_BOARD_H__
	#define __CORE_BOARD_H__

	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>
	#include <string.h>
	#include <errno.h>
	#include <time.h>
	#include <math.h>

	#include "../UserInterface/start_menu.h"
	#include "../UserInterface/board.h"
	#include "../Lib/List/list.h"
	#include "../Lib/Vector/vector.h"

	typedef enum {
		SQUARE,
		TRIANGLE,
		DIAMOND,
		CIRCLE,
		EMPTY
	} PieceIndex;

	typedef struct {
		ssize_t y;
		ssize_t x;
	} Coordinate;

	LIST(Coordinate, Coordinate)

	typedef enum {
		STATIONARY,
		JUMPED,
		WALKED
	} Displacement;

	typedef struct {
		bool motion[5][5];
	} MotionMask;

	typedef struct {
		PieceIndex index;
		Players owner;
		Coordinate position;
	} Cell;

	VECTOR(Cell *, Cell)

	typedef struct {
		Cell * board[BOARD_SQUARE_HEIGHT][BOARD_SQUARE_WIDTH];
		VectorCell * tracker[4];
	} CoreBoard;

	/**
	 * \fn static MotionMask default_circle_mask(void)
	 * \brief Returns default motion mask for circle piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:35 PM
	 * \return Default circle motion mask.
	 */
	static MotionMask default_circle_mask(void);

	/**
	 * \fn static MotionMask default_diamond_mask(void)
	 * \brief Returns default motion mask for diamond piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:37 PM
	 * \return Default diamond motion mask.
	 */
	static MotionMask default_diamond_mask(void);

	/**
	 * \fn static MotionMask default_triangle_mask(void)
	 * \brief Returns default motion mask for triangle piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:38 PM
	 * \return Default triangle motion mask.
	 */
	static MotionMask default_triangle_mask(void);

	/**
	 * \fn static MotionMask default_square_mask(void)
	 * \brief Returns default motion mask for square piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \return Default square motion mask.
	 */
	static MotionMask default_square_mask(void);

	/**
	 * \fn MotionMask default_mask_for(const PieceIndex index)
	 * \brief Returns the mask corresponding to the piece of a given index.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param param desc
	 * \return The mask of the piece of a given index.
	 */
	MotionMask default_mask_for(const PieceIndex index);

	/**
	 * \fn Cell * Cell_allocinit_empty(void)
	 * \brief Allocates and initializes a \a Cell to an empty state.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param param desc
	 * \return Newly allocated Cell object.
	 */
	Cell * Cell_allocinit_empty(void);

	/**
	 * \fn static void Cell_set_position(Cell * const cell, const size_t y, const size_t x)
	 * \brief Set the board position of a given \a Cell.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param cell A given cell.
	 * \param y Given new Y position.
	 * \param x Given new X position.
	 */
	static void Cell_set_position(Cell * const cell, const size_t y, const size_t x);

	/**
	 * \fn void Cell_set(Cell * const cell, const PieceIndex index, const Players owner, const size_t y, const size_t x)
	 * \brief Set a all fields of a \a Cell object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param cell A given Cell.
	 * \param index A given piece index.
	 * \param owner Owner of piece.
	 * \param y Given new Y position.
	 * \param x Given new X position.
	 */
	void Cell_set(Cell * const cell, const PieceIndex index, const Players owner, const size_t y, const size_t x);

	/**
	 * \fn static void Cell_free(Cell * * const cell)
	 * \brief Frees a given \a Cell.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param cell A given Cell pointer address.
	 */
	static void Cell_free(Cell * * const cell);

	/**
	 * \fn static CoreBoard * CoreBoard_allocinit_empty(void)
	 * \brief Allocates and initializes a \a CoreBoard to an empty state.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \return Newly allocated Cell object.
	 */
	static CoreBoard * CoreBoard_allocinit_empty(void);

	/**
	 * \fn void CoreBoard_free(CoreBoard * * const board, const NumberOfPlayer n)
	 * \brief Frees a \a CoreBoard object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param board A \a CoreBoard pointer address.
	 * \param n Number of players.
	 */
	void CoreBoard_free(CoreBoard * * const board, const NumberOfPlayer n);

	/**
	 * \fn static void CoreBoard_init_sides(CoreBoard * const board, const NumberOfPlayer n)
	 * \brief Initializes the sides of the board (Player pieces).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param board A given \a CoreBoard object.
	 * \param n Number of players.
	 */
	static void CoreBoard_init_sides(CoreBoard * const board, const NumberOfPlayer n);

	/**
	 * \fn static Coordinate Coordinate_diff(const Coordinate c0, const Coordinate c1)
	 * \brief Returns difference between two given coordinate.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param c0 A given coordinate.
	 * \param c1 A given coordinate.
	 * \return Difference between two coordinate.
	 */
	static Coordinate Coordinate_diff(const Coordinate c0, const Coordinate c1);

	/**
	 * \fn static bool Coordinate_is_reachable(const Coordinate what, const Coordinate where)
	 * \brief Tests if a given coordinate is within reach given a current position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param what Current position.
	 * \param where Position to reach.
	 * \return true if a given coordinate is reachable else false
	 */
	static bool Coordinate_is_reachable(const Coordinate what, const Coordinate where);

	/**
	 * \fn bool Coordinate_is_at_border_maskwise(const Coordinate c)
	 * \brief Tests if a mask position is at the border of its mask.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:48 PM
	 * \param c A given mask coordinate.
	 * \return true if a given mask coordinate is at the border of its mask.
	 */
	bool Coordinate_is_at_border_maskwise(const Coordinate c);

	/**
	 * \fn static bool Coordinate_is_at_border_boardwise(const Coordinate c)
	 * \brief Tests if a board position is at the border of its board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param c A given board coordinate.
	 * \return true if a given mask coordinate is at the border of the board.
	 */
	static bool Coordinate_is_at_border_boardwise(const Coordinate c);

	/**
	 * \fn bool Coordinate_is_in_player_zone(const Coordinate c, const size_t p)
	 * \brief Tests if a player piece is in its win zone.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param c Coordinate of a player piece.
	 * \param p Given Player id value.
	 * \return true if a player piece is in its win zone else false.
	 */
	bool Coordinate_is_in_player_zone(const Coordinate c, const size_t p);

	/**
	 * \fn static Coordinate Coordinate_from_boardwise_to_maskwise(const Coordinate center, const Coordinate c)
	 * \brief Converts a given board wise coordinate into a mask wise coordinate.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param center A given board wise coordinate to convert.
	 * \param c A board wise coordinate to convert.
	 * \return Converted Coordinate.
	 */
	static Coordinate Coordinate_from_boardwise_to_maskwise(const Coordinate center, const Coordinate c);

	/**
	 * \fn Coordinate Coordinate_from_maskwise_to_boardwise(const Coordinate center, const Coordinate c)
	 * \brief Converts a given mask wise coordinate to a board wise coordinate.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param center A given board wise coordinate to convert.
	 * \param c A mask wise coordinate to convert.
	 * \return Converted Coordinate.
	 */
	Coordinate Coordinate_from_maskwise_to_boardwise(const Coordinate center, const Coordinate c);

	/**
	 * \fn static bool Coordinate_is_within_coreboard(const Coordinate c)
	 * \brief Tests if a coordinate is within the board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param c A given coordinate.
	 * \return true if the coordinate is within the board else false.
	 */
	static bool Coordinate_is_within_coreboard(const Coordinate c);

	/**
	 * \fn static Coordinate Coordinate_middle_point(const Coordinate c0, const Coordinate c1)
	 * \brief Returns the point in between two given coordinate.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param c0 A given coordinate.
	 * \param c1 A given coordinate.
	 * \return Coordinate of the point in between the two given point.
	 */
	static Coordinate Coordinate_middle_point(const Coordinate c0, const Coordinate c1);

	/**
	 * \fn static bool MotionMask_can_move(const MotionMask mask, const size_t step)
	 * \brief Tests if there is at least one valid move in a given motion mask.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param mask A given motion mask.
	 * \param step Length of the loop step in a line when walking through the mask.
	 * \return true if there is at least one valid move in a given motion mask else false.
	 */
	static bool MotionMask_can_move(const MotionMask mask, const size_t step);

	/**
	 * \fn static void MotionMask_disallow_if_at_border(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t who, const size_t i, const size_t j)
	 * \brief Turns off motion value if given mask coordinate is at the board border.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param mask Given mask.
	 * \param board Given board.
	 * \param what Current position.
	 * \param who Player id.
	 * \param i Given y position within the mask.
	 * \param j Given x position within the mask.
	 */
	static void MotionMask_disallow_if_at_border(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t who, const size_t i, const size_t j);

	/**
	 * \fn static void MotionMask_disallow_if_unavailable(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t i, const size_t j)
	 * \brief Turns off motion value if given mask position lead to a taken \a Cell.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param mask Given mask.
	 * \param what Current position.
	 * \param i Given y position within the mask.
	 * \param j Given x position within the mask.
	 */
	static void MotionMask_disallow_if_unavailable(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t i, const size_t j);

	/**
	 * \fn static void MotionMask_disallow_if_middle_available(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t i, const size_t j)
	 * \brief Turns off motion value if there is a middle point available in between the center of the mask and the given mask position.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param mask Given mask.
	 * \param board Given board.
	 * \param what Current position.
	 * \param i Given y position within the mask.
	 * \param j Given x position within the mask.
	 */
	static void MotionMask_disallow_if_middle_available(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t i, const size_t j);

	/**
	 * \fn static bool MotionMask_is_termination_valid(ListCoordinate * const previousPositions, CoreBoard * const board, const PieceIndex index, const size_t who, const Coordinate currentPosition)
	 * \brief Tests if a current position leads to a valid termination.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param previousPositions Lists of all current previous positions. (Prevents from going in a loop).
	 * \param board Given board.
	 * \param index Shape / Piece index.
	 * \param who Player id.
	 * \param currentPosition Current position.
	 * \return true if there is at least on valid termination else false.
	 * A termination is the position at the end of series of jump(s). It is said to be valid if it is not at the board border.
	 */
	static bool MotionMask_is_termination_valid(ListCoordinate * const previousPositions, CoreBoard * const board, const PieceIndex index, const size_t who, const Coordinate currentPosition);

	/**
	 * \fn static void MotionMask_disallow_if_terminate_in_border(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const PieceIndex index, const size_t who, const size_t i, const size_t j)
	 * \brief Turns off motion value if termination is said invalid.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param mask Given mask.
	 * \param board Given board.
	 * \param what Current position.
	 * \param index Shape / Piece index.
	 * \param who Player id.
	 * \param i Given y position within the mask.
	 * \param j Given x position within the mask.
	 * A termination is the position at the end of series of jump(s). It is said to be valid if it is not at the board border.
	 */
	static void MotionMask_disallow_if_terminate_in_border(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const PieceIndex index, const size_t who, const size_t i, const size_t j);

	/**
	 * \fn void MotionMask_filter(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t who, const PieceIndex index)
	 * \brief Filters a given mask by applying rules.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param mask Given mask.
	 * \param board Given board.
	 * \param what Current position.
	 * \param who Player id.
	 * \param index Shape / Piece index.
	 */
	void MotionMask_filter(MotionMask * const mask, CoreBoard * const board, const Coordinate what, const size_t who, const PieceIndex index);

	/**
	 * \fn bool CoreBoard_is_owner(CoreBoard * const board, const Players who, const Coordinate what)
	 * \brief Tests if a given player is owner of a given piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param board Given board.
	 * \param what Current position.
	 * \param who Player id.
	 * \return true if a given player is owner of a given piece else false.
	 */
	bool CoreBoard_is_owner(CoreBoard * const board, const Players who, const Coordinate what);

	/**
	 * \fn bool CoreBoard_can_jump(CoreBoard * const board, const size_t who, const Coordinate what, ListCoordinate * const previousPositions)
	 * \brief Tests if a given piece can perform a jump motion.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param param desc
	 * \return true if a given piece can perform a jump motion else false.
	 */
	bool CoreBoard_can_jump(CoreBoard * const board, const size_t who, const Coordinate what, ListCoordinate * const previousPositions);

	/**
	 * \fn void CoreBoard_alter_board(CoreBoard * const board, const Coordinate what, const Coordinate where)
	 * \brief Switches \a Cell position in a given board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param board A given \a CoreBoard object.
	 * \param what Current position.
	 * \param where Position to reach.
	 */
	void CoreBoard_alter_board(CoreBoard * const board, const Coordinate what, const Coordinate where);

	/**
	 * \fn Displacement CoreBoard_move(CoreBoard * const board, const size_t who, const Coordinate what, const Coordinate where)
	 * \brief Moves a given piece at a given position in a given board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param board A given \a CoreBoard object.
	 * \param what Current position.
	 * \param where Position to reach.
	 * \return Motion type.
	 */
	Displacement CoreBoard_move(CoreBoard * const board, const size_t who, const Coordinate what, const Coordinate where);

	/**
	 * \fn bool CoreBoard_did_player_win(CoreBoard * const board, const size_t player)
	 * \brief Tests if a given player has won the game.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param board A given \a CoreBoard object.
	 * \param player Player id.
	 * \return true if a given player has won the game else false.
	 */
	bool CoreBoard_did_player_win(CoreBoard * const board, const size_t player);

	/**
	 * \fn bool CoreBoard_can_skip(CoreBoard * const board, const size_t who)
	 * \brief Tests if a given player can skip its turn.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param board A given \a CoreBoard object.
	 * \param who Player id.
	 * \return true if a given player can skip its turn else false.
	 */
	bool CoreBoard_can_skip(CoreBoard * const board, const size_t who);

	/**
	 * \fn static void CoreBoard_init_tracker(CoreBoard * const board, const NumberOfPlayer n)
	 * \brief Initializes \a Cell tracker of a given board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param board A given \a CoreBoard object.
	 * \param n Number of players.
	 */
	static void CoreBoard_init_tracker(CoreBoard * const board, const NumberOfPlayer n);

	/**
	 * \fn bool CoreBoard_is_any_piece_unmoved(CoreBoard * const board, const NumberOfPlayer n)
	 * \brief Tests if any player piece has not been moved since the beginning of the game.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-06 Wed 06:25 AM
	 * \param param desc
	 * \return true if any piece has not been moved else false.
	 */
	bool CoreBoard_is_any_piece_unmoved(CoreBoard * const board, const NumberOfPlayer n);

	/**
	 * \fn CoreBoard * CoreBoard_generate(const StartupSettings settings, size_t * const currentPlayerIndex, size_t * const roundCounter)
	 * \brief Generates a core board.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Sun 11:49 PM
	 * \param settings Startup settings
	 * \param currentPlayerIndex current player index.
	 * \param roundCounter round counter.
	 * \return Newly allocated and initialized \a CoreBoard object.
	 */
	CoreBoard * CoreBoard_generate(const StartupSettings settings, size_t * const currentPlayerIndex, size_t * const roundCounter);

	void disp(CoreBoard * b);

#endif
