/**
 * \file minimax.c
 * \brief This file contains the routines specific to the intelligent bot.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 06:54 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "minimax.h"


static void Coordinate_set_coord(Coordinate * const c, const Coordinate value)
{
	c->y = value.y;
	c->x = value.x;
}

static void Coordinate_set_scal(Coordinate * const c, const size_t y, const size_t x)
{
	c->y = y;
	c->x = x;
}

static Displacement next_displacement(const Coordinate maskChild)
{
	Displacement nextDisp;
	nextDisp = WALKED;
	if (Coordinate_is_at_border_maskwise(maskChild)) {
		nextDisp = JUMPED;
	}
	return nextDisp;
}

LIST_PUSH_FRONT(ID, ID)

static void Bot_add_history(ListID * const history, Cell * const cellId, const Coordinate footHole)
{
	ID id;
	id.cellId = cellId;
	id.footHole = footHole;
	ListID_push_front(history, id);
}

LISTELEMENT_ISTAILSENTINEL(ID, ID)
LIST_WHEREIS(ID, ID)

static size_t Bot_minimax_make_decision(CoreBoard * const coreBoard, const size_t who, const PieceIndex shape, const size_t depth, const Coordinate parent, const Displacement previousDisp, const size_t moveCounter, ListID * const history, Cell * const cellId)
{
	size_t result, i, j, nextMoveCounter, tmpResult;
	Coordinate child, maskChild;
	ID childID;
	Displacement nextDisp;
	MotionMask parentMask;
	if (Coordinate_is_in_player_zone(parent, who) || (moveCounter >= depth)) {
		result = moveCounter;
	} else {
		result = depth;
		parentMask = default_mask_for(shape);
		MotionMask_filter(&parentMask, coreBoard, parent, who, shape);
		Bot_add_history(history, cellId, parent);
		childID.cellId = cellId;
		for (i = 0; i < 5; i++) {
			for (j = 0; j < 5; j++) {
				if (parentMask.motion[i][j]) {
					Coordinate_set_scal(&maskChild, i, j);
					Coordinate_set_coord(&child, Coordinate_from_maskwise_to_boardwise(parent, maskChild));
					childID.footHole = child;
					if (ListElementID_istailsentinel(ListID_whereis(history, childID))) {
						nextDisp = next_displacement(maskChild);
						nextMoveCounter = moveCounter;
						if ((previousDisp != JUMPED) || (nextDisp != JUMPED)) {
							nextMoveCounter++;
						}
						tmpResult = Bot_minimax_make_decision(coreBoard, who, shape, depth, child, nextDisp, nextMoveCounter, history, cellId);
						if (tmpResult < result) {
							result = tmpResult;
						}
					}
				}
			}
		}
	}
	return result;
}

static Where Bot_determine_where(CoreBoard * const coreBoard, const size_t who, const PieceIndex shape,  const Coordinate what, const size_t depth, ListID * const history, Cell * const piece)
{
	Where where;
	MotionMask mask;
	Coordinate maskWise, boardWise;
	size_t i, j, counter;
	Displacement nextDisp;
	mask = default_mask_for(shape);
	MotionMask_filter(&mask, coreBoard, what, who, shape);
	where.moveCounter = depth;
	where.disp = STATIONARY;
	where.where = what;
	Bot_add_history(history, piece, what);
	for (i = 0; i < 5; i++) {
		for (j = 0; j < 5; j++) {
			if (mask.motion[i][j]) {
				Coordinate_set_scal(&maskWise, i, j);
				Coordinate_set_coord(&boardWise, Coordinate_from_maskwise_to_boardwise(what, maskWise));
				nextDisp = next_displacement(maskWise);
				counter = Bot_minimax_make_decision(coreBoard, who, shape, depth, boardWise, nextDisp, 1, history, piece);
				if (counter <= where.moveCounter) {
					where.moveCounter = counter;
					where.disp = nextDisp;
					where.where = boardWise;
				}
			}
		}
	}
	return where;
}


VECTOR_GET(Cell *, Cell)
LIST_MALLOC(ID, ID)
LIST_FREE(ID, ID)

BotAction Bot_move(CoreBoard * const coreBoard, const size_t who, const size_t depth)
{
	BotAction action;
	Cell * piece;
	PieceIndex shape;
	Coordinate what;
	ListID * history;
	Where where;
	size_t i, counter;
	i = 0;
	do {
		piece = VectorCell_get(coreBoard->tracker[who], i);
		action.what = piece->position;
		i++;
	} while ((i < 8) && Coordinate_is_in_player_zone(action.what, who));
	shape = piece->index;
	history = ListID_malloc();
	where = Bot_determine_where(coreBoard, who, shape, action.what, depth, history, piece);
	ListID_free(&history);
	action.where = where.where;
	action.disp = where.disp;
	counter = where.moveCounter;
	for (i = 1; i < 8; i++) {
		piece = VectorCell_get(coreBoard->tracker[who], i);
		history = ListID_malloc();
		what = piece->position;
		shape = piece->index;
		where = Bot_determine_where(coreBoard, who, shape, what, depth, history, piece);
		if (where.moveCounter < counter) {
			counter = where.moveCounter;
			action.what = what;
			action.where = where.where;
			action.disp = where.disp;
		}
		ListID_free(&history);
	}
	if (action.disp != STATIONARY) {
		CoreBoard_alter_board(coreBoard, action.what, action.where);
	}
	return action;
}
