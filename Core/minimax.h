/**
 * \file minimax.h
 * \brief This file contains headers of the routines specific to the intelligent bot.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 06:54 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __MINIMAX_H__
	#define __MINIMAX_H__
	
	#include <stdio.h>
	#include <stdlib.h>

	#include "coreboard.h"
	#include "../Lib/Vector/vector.h"
	#include "../Lib/List/list.h"

	typedef struct {
		Coordinate what;
		Coordinate where;
		Displacement disp;
	} BotAction;

	typedef struct {
		Coordinate where;
		size_t moveCounter;
		Displacement disp;
	} Where;

	typedef struct {
		Cell * cellId;
		Coordinate footHole;
	} ID;

	LIST(ID, ID)

	/**
	 * \fn static void Coordinate_set_coord(Coordinate * const c, const Coordinate value)
	 * \brief Sets coordinate from a \a Coordinate typed value.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 06:55 AM
	 * \param c Coordinate to set.
	 * \param value A given coordinate value.
	 */
	static void Coordinate_set_coord(Coordinate * const c, const Coordinate value);

	/**
	 * \fn static void Coordinate_set_scal(Coordinate * const c, const size_t y, const size_t x)
	 * \brief Sets Coordinate from a two scalar values.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 06:56 AM
	 * \param c Coordinate to set.
	 * \param y Given y scalar.
	 * \param x Given x scalar.
	 */
	static void Coordinate_set_scal(Coordinate * const c, const size_t y, const size_t x);

	/**
	 * \fn static Displacement next_displacement(const Coordinate maskChild)
	 * \brief Determines next displacement type.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 06:56 AM
	 * \param mask Given mask child coordinate.
	 * \return Type of displacement.
	 */
	static Displacement next_displacement(const Coordinate maskChild);

	/**
	 * \fn static void Bot_add_history(ListID * const history, Cell * const cellId, const Coordinate footHole)
	 * \brief Adds a position to the history list of bot motions.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 06:56 AM
	 * \param history A given list representing an history.
	 * \param cellId Address of the cell at which motion occurred.
	 * \param history A given list representing an history.
	 */
	static void Bot_add_history(ListID * const history, Cell * const cellId, const Coordinate footHole);

	/**
	 * \fn static size_t Bot_minimax_make_decision(CoreBoard * const coreBoard, const size_t who, const PieceIndex shape, const size_t depth, const Coordinate parent, const Displacement previousDisp, const size_t moveCounter, ListID * const history, Cell * const cellId)
	 * \brief Runs minimax algorithm in order to determine the direction to take for one piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 06:56 AM
	 * \param param desc
	 * \return The minimal number of move it takes for one piece to arrive in its end zone.
	 */
	static size_t Bot_minimax_make_decision(CoreBoard * const coreBoard, const size_t who, const PieceIndex shape, const size_t depth, const Coordinate parent, const Displacement previousDisp, const size_t moveCounter, ListID * const history, Cell * const cellId);
	/**
	 * \fn static Where Bot_determine_where(CoreBoard * const coreBoard, const size_t who, const PieceIndex shape,  const Coordinate what, const size_t depth, ListID * const history, Cell * const piece)
	 * \brief Determines where to move for one piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 06:56 AM
	 * \param coreBoard A given \a CoreBoard object.
	 * \param who Player ID.
	 * \param shape Shape / Pieces index.
	 * \param what Current position
	 * \param depth Depth of the recursive search;
	 * \param history A given list representing an history.
	 * \param piece Concerned \a Cell.
	 * \return Determines where to move for a given piece.
	 */
	static Where Bot_determine_where(CoreBoard * const coreBoard, const size_t who, const PieceIndex shape,  const Coordinate what, const size_t depth, ListID * const history, Cell * const piece);

	/**
	 * \fn BotAction Bot_move(CoreBoard * const coreBoard, const size_t who, const size_t depth)
	 * \brief Decides what piece to move as a given player.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 06:56 AM
	 * \param coreBoard A given \a CoreBoard object.
	 * \param who Player ID.
	 * \param depth Depth of the recursive search;
	 * \return Bot action.
	 */
	BotAction Bot_move(CoreBoard * const coreBoard, const size_t who, const size_t depth);
#endif
