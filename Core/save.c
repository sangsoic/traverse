#include "save.h"

static void mkdir_data(void)
{
	if (access("Data", F_OK) == -1) {
		mkdir("Data", 0700);
	}
}

static char * get_valid_save_name(void) { char * name;
	do {
		name = secure_inputstr("Enter save name: ");
	} while (strlen(name) == 0);
	return name;
}

static void fwrite_board_state(const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty, FILE * const save)
{
	int cursor;
	Cell * cell;
	size_t i, j;
	if ((fputc((int)currentPlayerIndex, save) == EOF) || 
	(fputc((int)roundCounter, save) == EOF) ||
	(fputc((int)numberOfPlayer, save) == EOF) ||
	(fputc((int)botNumber, save) == EOF) ||
	(fputc((int)difficulty, save) == EOF)) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			cell = coreBoard->board[i][j];
			cursor = Players_index_for(cell->owner) * 5 + cell->index;
			if (fputc(cursor, save) == EOF) {
				fprintf(stderr, "error : %s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
		}
	}
	for (i = 0; i < numberOfPlayer; i++) {
		for (j = 0; j < 8; j++) {
			cell = coreBoard->tracker[i]->value[j];
			cursor = cell->position.y * 10 + cell->position.x;
			if (fputc(cursor, save) == EOF) {
				fprintf(stderr, "error : %s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
		}
	}
}

void save_board_state(const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty)
{
	FILE * save;
	char * path, * name;
	path = name = NULL;
	mkdir_data();
	name = get_valid_save_name();
	if ((path = malloc((strlen(name) + 6) * sizeof(char))) == NULL) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	sprintf(path, "Data/%s", name);
	if ((save = fopen(path, "w")) == NULL) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	fwrite_board_state(coreBoard, currentPlayerIndex, roundCounter, numberOfPlayer, botNumber, difficulty, save);
	free(name);
	free(path);
	if (fclose(save) == EOF) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

VECTOR_MALLOC(Cell *, Cell)
VECTOR_PUSH_BACK(Cell *, Cell)

static CoreBoard * _restore_board_state(FILE * const save, const NumberOfPlayer numberOfPlayer)
{
	CoreBoard * coreBoard;
	Cell * cell;
	unsigned char cursor;
	size_t i, j;
	if ((coreBoard = malloc(sizeof(CoreBoard))) == NULL) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			cell = Cell_allocinit_empty();
			if ((cursor = fgetc(save)) == EOF) {
				fprintf(stderr, "error : %s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
			Cell_set(
				cell, 
				cursor % 5,
				Players_symbol_for(cursor / 5),
				i, j
			);
			coreBoard->board[i][j] = cell;
		}
	}
	for (i = 0; i < numberOfPlayer; i++) {
		coreBoard->tracker[i] = VectorCell_malloc(8);
		for (j = 0; j < 8; j++) {
			if ((cursor = fgetc(save)) == EOF) {
				fprintf(stderr, "error : %s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
			VectorCell_push_back(coreBoard->tracker[i], coreBoard->board[cursor / 10][cursor % 10]);
		}
	}
	return coreBoard;
}

static void redraw_screen_state(Board * const board, const CoreBoard * const coreBoard, const NumberOfPlayer numberOfPlayer)
{
	size_t i, j;
	Cell * cell;
	void (* redraw[4])(Board * const, const int, const int, const Players);
	redraw[0] = Square_draw_square;
	redraw[1] = Square_draw_triangle;
	redraw[2] = Square_draw_diamond;
	redraw[3] = Square_draw_circle;
	for (i = 0; i < numberOfPlayer; i++) {
		for (j = 0; j < 8; j++) {
			cell = coreBoard->tracker[i]->value[j];
			redraw[cell->index](board, cell->position.y, cell->position.x, cell->owner);
		}
	}
}

Board * restore_screen(const CoreBoard * const coreBoard, const short theme, const NumberOfPlayer numberOfPlayer)
{
	Board * board;
	int lines, cols;
	getmaxyx(stdscr, lines, cols);
	board = Board_allocate((lines - BOARD_SCREEN_HEIGHT) / 2 + 1, (cols - BOARD_SCREEN_WIDTH) / 2);
	init_pair(1, COLOR_BLACK, COLOR_WHITE);
	init_pair(2, COLOR_WHITE, theme);
	Board_init(board);
	redraw_screen_state(board, coreBoard, numberOfPlayer);
	return board;
}

BoardState restore_board_state(const char * const path, const short theme)
{
	BoardState state;
	FILE * save;
	if ((save = fopen(path, "r")) == NULL) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	state.currentPlayerIndex = (size_t)fgetc(save);
	state.roundCounter = (size_t)fgetc(save);
	state.numberOfPlayer = (NumberOfPlayer)fgetc(save);
	state.botNumber = (short)fgetc(save);
	state.difficulty = (short)fgetc(save);
	state.coreBoard = _restore_board_state(save, state.numberOfPlayer);
	state.board = restore_screen(state.coreBoard, theme, state.numberOfPlayer);
	if (fclose(save) == EOF) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return state;
}
