/**
 * \file save.h
 * \brief This file contains routines that manipulates save files.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Sun 11:50 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __SAVE_H__
	#define  __SAVE_H__
	
	#include "coreboard.h"
	#include "terminal_io.h"
	#include "../UserInterface/board.h"

	#include <unistd.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <string.h>
	#include <ncurses.h>

	typedef struct {
		CoreBoard * coreBoard;
		size_t currentPlayerIndex;
		size_t roundCounter;
		NumberOfPlayer numberOfPlayer;
		short botNumber;
		short difficulty;
		Board * board;
	} BoardState;

	/**
	 * \fn static void mkdir_data(void)
	 * \brief Makes the Data directory if it does not already exist.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:39 AM
	 */
	static void mkdir_data(void);

	/**
	 * \fn static char * get_valid_save_name(void)
	 * \brief Gets a non empty string.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:40 AM
	 * \return Non empty string.
	 */
	static char * get_valid_save_name(void);

	/**
	 * \fn static void fwrite_board_state(const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty, FILE * const save)
	 * \brief Writes the current board state in a save file.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:41 AM
	 * \param coreBoard A \a CoreBoard object.
	 * \param currentPlayerIndex Current player index.
	 * \param roundCounter Round counter.
	 * \param numberOfPlayer Number of player.
	 * \param botNumber Bot Number.
	 * \param difficulty Bot Difficulty.
	 * \param save Save file.
	 */
	static void fwrite_board_state(const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty, FILE * const save);

	/**
	 * \fn void save_board_state(const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty)
	 * \brief Saves the current board state (Calls \a fwrite_board_state).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:46 AM
	 * \param coreBoard A \a CoreBoard object.
	 * \param currentPlayerIndex Current player index.
	 * \param roundCounter Round counter.
	 * \param numberOfPlayer Number of player.
	 * \param botNumber Bot Number.
	 * \param difficulty Bot Difficulty.
	 */
	void save_board_state(const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty);

	/**
	 * \fn static void redraw_screen_state(Board * const board, const CoreBoard * const coreBoard, const NumberOfPlayer numberOfPlayer)
	 * \brief Redraws saved screen.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:48 AM
	 * \param board A \a Board object.
	 * \param coreBoard A \a CoreBoard object.
	 * \param numberOfPlayer Number of player.
	 */
	static void redraw_screen_state(Board * const board, const CoreBoard * const coreBoard, const NumberOfPlayer numberOfPlayer);

	/**
	 * \fn static CoreBoard * _restore_board_state(FILE * const save, const NumberOfPlayer numberOfPlayer)
	 * \brief Reads data from save file.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:51 AM
	 * \param save Save file.
	 * \param numberOfPlayer Number of player.
	 * \return Restored \a CoreBoard from save file.
	 */
	static CoreBoard * _restore_board_state(FILE * const save, const NumberOfPlayer numberOfPlayer);

	/**
	 * \fn Board * restore_screen(const CoreBoard * const coreBoard, const short theme, const NumberOfPlayer numberOfPlayer)
	 * \brief Restores screen state.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:54 AM
	 * \param coreBoard A \a CoreBoard object.
	 * \param theme Board theme.
	 * \param numberOfPlayer Number of player.
	 * \return \a Board objects.
	 */
	Board * restore_screen(const CoreBoard * const coreBoard, const short theme, const NumberOfPlayer numberOfPlayer);

	/**
	 * \fn BoardState restore_board_state(const char * const path, const short theme)
	 * \brief Restores board state (Calls \a restore_screen and \a _restore_board_state).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:58 AM
	 * \param path Path of a save file.
	 * \param theme Board theme.
	 * \return \a BoardState objects which regroups all data restored from a save file.
	 */
	BoardState restore_board_state(const char * const path, const short theme);

#endif
