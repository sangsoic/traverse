# The original Traverse game.

![traverse game](Img/board.png)

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

Traverse is played on a square "checker" game board. Each player chooses one color of playing pieces. Each player has eight pieces of the same color: 2 squares, 2 diamonds, 2 triangles, and 2 circles. Each player arranges their pieces in ANY ORDER within their starting row. The object of the game is to get ALL of your pieces into the starting row of the player sitting opposite of you. The pieces move in different directions depending upon the shape:

* Squares can move horizontally and vertically.
* Diamonds can move only diagonally.
* Triangles can move forward on the diagonals or straight backwards.
* Circles can move in any direction.

Players take turns moving one piece each turn. Two pieces can not occupy the same space. Pieces can move in single space moves, one space at a time and only into an empty adjacent space as dictated by the piece's legal moves. Players can jump over their own or another player's piece. Jumped pieces are NOT captured, akin to Chinese Checkers. Players can string together a series of jumps if each individual jump in the series conforms to the rules governing single jumps. The first player to move all of their pieces into the destination row is the winner. Players can not force a draw.

*This description comes from [here](https://boardgamegeek.com/boardgame/3313/traverse).*

## How to clone, compile and execute

NCURSES is a dependency of this project. [Here](https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/intro.html#WHERETOGETIT) are instructions on how to install it.

### Clone.

This project comes with submodules. To clone both, submodules and the project, execute the following command.

With version 2.13 of Git and later, --recurse-submodules can be used instead of --recursive:

```
git clone --recurse-submodules -j8 https://gitlab.com/sangsoic/traverse.git
cd traverse
```

Editor’s note: -j8 is an optional performance optimization that became available in version 2.8, and fetches up to 8 submodules at a time in parallel — see man git-clone.

Or with version 1.9 of Git up until version 2.12 (-j flag only available in version 2.8+):

`git clone --recursive -j8 https://gitlab.com/sangsoic/traverse.git`

Or with version 1.6.5 of Git and later, you can use:

`git clone --recursive https://gitlab.com/sangsoic/traverse.git`

Or for already cloned repos, or older Git versions, use:

```
git clone https://gitlab.com/sangsoic/traverse.git
cd traverse
git submodule update --init --recursive
```

*for more information on cloning submodules click [here](https://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules)*

### Compile.

`make`

### Execute.

`./traverse`

