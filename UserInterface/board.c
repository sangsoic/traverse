/**
 * \file board.h
 * \brief This file contains objects that perform graphical text operation on the game board.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-24 Fri 03:41 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "board.h"

Board * Board_allocate(const int atY, const int atX)
{
	Board * board;
	int i, j;
	board = malloc(sizeof(Board));
	if (board == NULL) {
		endwin();
		fprintf(stderr, "%s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	board->whole = newwin(BOARD_SCREEN_HEIGHT, BOARD_SCREEN_WIDTH, atY, atX);
	if (board->whole == NULL) {
		endwin();
		fprintf(stderr, "error ncurses : cannot allocate a window.\n");
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < BOARD_SCREEN_HEIGHT; i += SQUARE_SCREEN_HEIGHT) {
		for (j = 0; j < BOARD_SCREEN_WIDTH; j += SQUARE_SCREEN_WIDTH) {
			board->squares[i / SQUARE_SCREEN_HEIGHT][j / SQUARE_SCREEN_WIDTH] = derwin(board->whole, SQUARE_SCREEN_HEIGHT, SQUARE_SCREEN_WIDTH, i, j);
			if (board->squares[i / SQUARE_SCREEN_HEIGHT][j / SQUARE_SCREEN_WIDTH] == NULL) {
				endwin();
				fprintf(stderr, "error ncurses : cannot allocate a window.\n");
				exit(EXIT_FAILURE);
			}
		}
	}
	return board;
}

void Board_check_screensize(void)
{
	int lines, cols;
	getmaxyx(stdscr, lines, cols);
	while ((lines < (BOARD_SCREEN_HEIGHT + 1)) || (cols < BOARD_SCREEN_WIDTH)) {
		move(0, 0);
		clrtoeol();
		printw("Your terminal size is currently %dx%d, please adjust it in real time until it reaches at least %dx%d.\n", lines, cols,BOARD_SCREEN_HEIGHT+1, BOARD_SCREEN_WIDTH);
		refresh();
		getch();
		getmaxyx(stdscr, lines, cols);
	}
	move(0, 0);
	clear();
}

static short Board_color(int y, int x)
{
	return ((x + (y % 2)) % 2) + 1;
}

Players Players_symbol_for(const size_t index)
{
	return PLAYER_0 + index;
}

size_t Players_index_for(const Players symbol)
{
	return symbol - PLAYER_0;
}

static void Square_draw_empty(Board * const board, const int i, const int j)
{
	int k;
	for (k = 0; k < SQUARE_SCREEN_HEIGHT; k++) {
		mvwhline(board->squares[i][j], k, 0, ' ', SQUARE_SCREEN_WIDTH);
	}
	wmove(board->squares[i][j], 0, 0);
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[i][j]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Square_draw_square(Board * const board, const int y, const int x, const Players symbol)
{
	mvwaddch(board->squares[y][x], 0, 0, symbol);
	mvwaddstr(board->squares[y][x], 1, 1, "|^^|");
	mvwaddstr(board->squares[y][x], 2, 1, "|__|");
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Square_draw_circle(Board * const board, const int y, const int x, const Players symbol)
{
	mvwaddch(board->squares[y][x], 0, 0, symbol);
	mvwaddstr(board->squares[y][x], 1, 1, "/''\\");
	mvwaddstr(board->squares[y][x], 2, 1, "\\__/");
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Square_draw_triangle(Board * const board, const int y, const int x, const Players symbol)
{
	mvwaddch(board->squares[y][x], 0, 0, symbol);
	mvwaddstr(board->squares[y][x], 1, 2, "/\\");
	mvwaddstr(board->squares[y][x], 2, 1, "/__\\");
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Square_draw_diamond(Board * const board, const int y, const int x, const Players symbol)
{
	mvwaddch(board->squares[y][x], 0, 0, symbol);
	mvwaddstr(board->squares[y][x], 1, 2, "/\\");
	mvwaddstr(board->squares[y][x], 2, 2, "\\/");
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Board_init(Board * const board)
{
	int i, j;
	short currentColorPair;
	refresh();
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			currentColorPair = Board_color(i, j);
			wattron(board->squares[i][j], COLOR_PAIR(currentColorPair) | A_BOLD);
			Square_draw_empty(board, i, j);
		}
	}
}

static void Board_init_sides(Board * const board, const NumberOfPlayer n)
{
	int i;
	void (* peices[4])(Board * const, const int, const int, const Players);
	peices[0] = Square_draw_square;
	peices[1] = Square_draw_triangle;
	peices[2] = Square_draw_diamond;
	peices[3] = Square_draw_circle;
	for (i = 0; i < 4; i++) {
		peices[i](board, 0, i + 1, PLAYER_0);
		peices[i](board, 0, (BOARD_SQUARE_WIDTH - 2) - i, PLAYER_0);
		peices[i](board, 9, i + 1, PLAYER_1);
		peices[i](board, 9, (BOARD_SQUARE_WIDTH - 2) - i, PLAYER_1);
	}
	if (n >= THREE_PLAYER) {
		for (i = 0; i < 4; i++) {
			peices[i](board, i + 1, 0, PLAYER_2);
			peices[i](board, (BOARD_SQUARE_WIDTH - 2) - i, 0, PLAYER_2);
		}
		if (n == FOUR_PLAYER) {
			for (i = 0; i < 4; i++) {
				peices[i](board, i + 1, 9, PLAYER_3);
				peices[i](board, (BOARD_SQUARE_WIDTH - 2) - i, 9, PLAYER_3);
			}
		}
	}
}

Board * Board_generate(const StartupSettings settings)
{
	Board * board;
	int lines, cols;
	getmaxyx(stdscr, lines, cols);
	board = Board_allocate((lines - BOARD_SCREEN_HEIGHT) / 2 + 1, (cols - BOARD_SCREEN_WIDTH) / 2);
	init_pair(1, COLOR_BLACK, COLOR_WHITE);
	init_pair(2, COLOR_WHITE, settings.theme);
	Board_init(board);
	Board_init_sides(board, settings.n);
	return board;
}

void Board_select(Board * const board, const int y, const int x)
{
	int i;
	short currentColorPair;
	currentColorPair = Board_color(y, x);
	for (i = 1; i < 3; i++) {
		mvwchgat(board->squares[y][x], i, 1, 4, A_BLINK | A_DIM, currentColorPair, NULL);
	}
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Board_move(Board * const board, const int y, const int x)
{
	chtype symbol;
	symbol = mvwinch(board->squares[y][x], 0, 0);
	wborder(board->squares[y][x], 0,0,0,0, symbol, symbol, symbol, symbol);
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Board_restore(Board * const board, const int y, const int x)
{
	chtype symbol;
	symbol = mvwinch(board->squares[y][x], 0, 0);
	wborder(board->squares[y][x], ' ', ' ', ' ', ' ', symbol, ' ', ' ', ' ');
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Board_unselect(Board * const board, const int y, const int x)
{
	int i;
	short currentColorPair;
	currentColorPair = Board_color(y, x);
	for (i = 1; i < 3; i++) {
		mvwchgat(board->squares[y][x], i, 1, 4, A_NORMAL | A_BOLD, currentColorPair, NULL);
	}
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y][x]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void Board_move_piece(Board * const board, const int y0, const int x0, const int y1, const int x1)
{
	char lineBuff[5];
	chtype symbol;
	int i;
	symbol = mvwinch(board->squares[y0][x0], 0, 0);
	mvwaddch(board->squares[y1][x1], 0, 0, (char)symbol);
	for (i = 1; i < 3; i++) {
		mvwinnstr(board->squares[y0][x0], i, 1, lineBuff, 4);
		mvwaddnstr(board->squares[y1][x1], i, 1, lineBuff, 4);
	}
	Square_draw_empty(board, y0, x0);
	if (touchwin(board->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot touchwin a window.\n");
		exit(EXIT_FAILURE);
	}
	if (wrefresh(board->squares[y1][x1]) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void BoardCursor_unselect(BoardCursor * const cursor)
{
	cursor->savedY = -1;
	cursor->savedX = -1;
}

void BoardCursor_init(BoardCursor * const cursor)
{
	cursor->currentY = 0;
	cursor->currentX = 0;
	BoardCursor_unselect(cursor);
}

void BoardCursor_select(BoardCursor * const cursor)
{
	cursor->savedY = cursor->currentY;
	cursor->savedX = cursor->currentX;
}

bool BoardCursor_isselected(BoardCursor * const cursor)
{
	return (cursor->savedY != -1) && (cursor->savedX != -1);
}

void BoardCursor_move(BoardCursor * const cursor, const int y, const int x)
{
	cursor->currentY = y;
	cursor->currentX = x;
}

void BoardCursor_move_down(BoardCursor * const cursor)
{
	cursor->currentY = (cursor->currentY + 1) % 10;
}

void BoardCursor_move_right(BoardCursor * const cursor)
{
	cursor->currentX = (cursor->currentX + 1) % 10;
}

void BoardCursor_move_up(BoardCursor * const cursor)
{
	cursor->currentY -= 1;
	cursor->currentY = ((9 - cursor->currentY) / 10) * 10 + cursor->currentY;
}

void BoardCursor_move_left(BoardCursor * const cursor) 
{
	cursor->currentX -= 1;
	cursor->currentX = ((9 - cursor->currentX) / 10) * 10 + cursor->currentX;
}

void Board_free(Board * * const board)
{
	Board * tmp;
	int i, j;
	tmp = *board;
	for (i = 0; i < BOARD_SQUARE_HEIGHT; i++) {
		for (j = 0; j < BOARD_SQUARE_WIDTH; j++) {
			if (delwin(tmp->squares[i][j]) == ERR) {
				endwin();
				fprintf(stderr, "error ncurses : cannot delete window.\n");
				exit(EXIT_FAILURE);
			}
		}
	}
	if (delwin(tmp->whole) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot delete window.\n");
		exit(EXIT_FAILURE);
	}
	free(tmp);
	*board = NULL;
}
