/**
 * \file event.c
 * \brief This file contains routine specific to mouse and keyboard events.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 09:13 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "event.h"

void Event_init_mouse(void)
{
	//if (has_mouse()) {
		mousemask(BUTTON1_CLICKED | BUTTON1_DOUBLE_CLICKED | BUTTON3_CLICKED, NULL);
	//}
}

void Event_init_keyboard(void)
{
	raw();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);
}

static void Event_handle_enter(GameEvent * const event)
{
	if (BoardCursor_isselected(&event->cursor)) {
		event->option = OPTION_MOVE_PIECE;
	} else {
		event->option = OPTION_SELECT;
		BoardCursor_select(&event->cursor);
	}
}

static void Event_handle_double_click(GameEvent * const event)
{
	if (BoardCursor_isselected(&event->cursor)) {
		event->option = OPTION_MOVE_CURSOR_AND_PIECE;
	} else {
		event->option = OPTION_MOVE_CURSOR_AND_SELECT;
		BoardCursor_select(&event->cursor);
	}
}

static void Event_handle_u_and_right_click(GameEvent * const event)
{
	event->option = OPTION_NONE;
	if (BoardCursor_isselected(&event->cursor)) {
		event->option = OPTION_UNSELECT;
	}
}

static void Event_handle_mouse(GameEvent * const event, WINDOW * const board, WINDOW * const bar)
{
	MEVENT mouseEvent;
	int rest;
	event->option = OPTION_NONE;
	if (getmouse(&mouseEvent) == OK) {
		if (wmouse_trafo(board, &mouseEvent.y, &mouseEvent.x, FALSE) == TRUE) {
			mouseEvent.y /= 4;
			mouseEvent.x /= 6;
			if (mouseEvent.bstate & BUTTON1_CLICKED) {
				BoardCursor_move(&event->cursor, mouseEvent.y, mouseEvent.x);
				event->option = OPTION_MOVE_CURSOR;
			} else if (mouseEvent.bstate & BUTTON1_DOUBLE_CLICKED) {
				BoardCursor_move(&event->cursor, mouseEvent.y, mouseEvent.x);
				Event_handle_double_click(event);
			} else if (mouseEvent.bstate & BUTTON3_CLICKED) {
				Event_handle_u_and_right_click(event);
			}
		} else if (wmouse_trafo(bar, &mouseEvent.y, &mouseEvent.x, FALSE) == TRUE) {
			mouseEvent.x += 1;
			rest = (mouseEvent.x % 10);
			mouseEvent.x /= 10;
			if ((rest != 0) && (mouseEvent.x <= 4)){
				event->option = mouseEvent.x;
			}
		}
	}
}

void Event_get_event(GameEvent * const event, WINDOW * const board, WINDOW * const bar)
{
	int key;
	key = getch();
	switch (key) {
		case KEY_MOUSE:
			Event_handle_mouse(event, board, bar);
			break;
		case KEY_UP:
			event->option = OPTION_MOVE_CURSOR;
			BoardCursor_move_up(&event->cursor);
			break;
		case KEY_DOWN:
			event->option = OPTION_MOVE_CURSOR;
			BoardCursor_move_down(&event->cursor);
			break;
		case KEY_LEFT:
			event->option = OPTION_MOVE_CURSOR;
			BoardCursor_move_left(&event->cursor);
			break;
		case KEY_RIGHT:
			event->option = OPTION_MOVE_CURSOR;
			BoardCursor_move_right(&event->cursor);
			break;
		case '\n':
			Event_handle_enter(event);
			break;
		case 'u':
			Event_handle_u_and_right_click(event);
			break;
		case 'p':
			event->option = OPTION_PASS;
			break;
		case 'h':
			event->option = OPTION_HELP;
			break;
		case 's':
			event->option = OPTION_SAVE;
			break;
		case 'q':
			event->option = OPTION_QUIT;
			break;
		case 'r':
			event->option = OPTION_RESTART;
			break;
		default:
			event->option = OPTION_NONE;
			break;
	}
}
