/**
 * \file event.h
 * \brief This file contains routine specific to mouse and keyboard events.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 09:13 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __EVENT_H__
	#define __EVENT_H__

	#include <stdio.h>
	#include <stdlib.h>
	#include <errno.h>
	#include <string.h>
	#include <ncurses.h>
	#include <stdbool.h>
	
	#include "board.h"
	#include "status_bar.h"

	typedef enum {
		OPTION_PASS,
		OPTION_HELP,
		OPTION_SAVE,
		OPTION_QUIT,
		OPTION_RESTART,
		OPTION_NONE,
		OPTION_MOVE_CURSOR_AND_SELECT,
		OPTION_MOVE_CURSOR_AND_PIECE,
		OPTION_SELECT,
		OPTION_UNSELECT,
		OPTION_MOVE_PIECE,
		OPTION_MOVE_CURSOR
	} Option;

	typedef struct {
		BoardCursor cursor;
		Option option;
	} GameEvent;

	/**
	 * \fn void Event_init_mouse(void)
	 * \brief Activates mouse events.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:15 AM
	 */
	void Event_init_mouse(void);

	/**
	 * \fn void Event_init_mouse(void)
	 * \brief Activates keyboard events.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:15 AM
	 */
	void Event_init_keyboard(void);

	/**
	 * \fn static void Event_handle_double_click(GameEvent * const event)
	 * \brief Handles double click functions.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:17 AM
	 * \param event A given \a GameEvent object.
	 */
	static void Event_handle_double_click(GameEvent * const event);

	/**
	 * \fn static void Event_handle_u_and_right_click(GameEvent * const event)
	 * \brief Handles u key and right unselection functions.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:20 AM
	 * \param event A given \a GameEvent object.
	 */
	static void Event_handle_u_and_right_click(GameEvent * const event);

	/**
	 * \fn static void Event_handle_enter(GameEvent * const event)
	 * \brief Handles enter key selection functions.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:22 AM
	 * \param event A given \a GameEvent object.
	 */
	static void Event_handle_enter(GameEvent * const event);

	/**
	 * \fn static void Event_handle_mouse(GameEvent * const event, WINDOW * const board, WINDOW * const bar)
	 * \brief Handles mouse functions.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:26 AM
	 * \param event A given \a GameEvent object.
	 * \param board A given board \a WINDOW.
	 * \param bar A given status bar \a WINDOW.
	 */
	static void Event_handle_mouse(GameEvent * const event, WINDOW * const board, WINDOW * const bar);

	/**
	 * \fn void Event_get_event(GameEvent * const event, WINDOW * const board, WINDOW * const bar)
	 * \brief Converts ncurses events into \a GameEvent.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:28 AM
	 * \param event A given \a GameEvent object.
	 * \param board A given board \a WINDOW.
	 * \param bar A given status bar \a WINDOW.
	 * GameEvent are a set of actions that the user can perform into the game.
	 */
	void Event_get_event(GameEvent * const event, WINDOW * const board, WINDOW * const bar);

#endif
