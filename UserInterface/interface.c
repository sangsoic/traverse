/**
 * \file interface.c
 * \brief This file contains routines related to the interface of the game.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 10:31 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "interface.h"

static void UI_init_game(StartupSettings * const settings, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * const currentEvent, size_t * const currentPlayer, bool * const isLocked, size_t * const roundCounter, Displacement * const currentDisp)
{
	BoardState state;
	initscr();
	if (! has_colors()) {
		endwin();
		fprintf(stderr, "error ncurses : your terminal has no color, colors are needed to play this game.\n");
		exit(EXIT_FAILURE);
	}
	Event_init_keyboard();
	start_color();
	Event_init_mouse();
	Board_check_screensize();
	if (settings->fileName == NULL) {
		*coreBoard = CoreBoard_generate(*settings, currentPlayer, roundCounter);
		*board = Board_generate(*settings);
	} else {
		state = restore_board_state(settings->fileName, settings->theme);
		*coreBoard = state.coreBoard;
		*currentPlayer = state.currentPlayerIndex;
		*roundCounter = state.roundCounter;
		settings->n = state.numberOfPlayer;
		settings->b = state.botNumber;
		settings->difficulty = state.difficulty;
		*board = state.board;
	}
	*bar = StatusBar_generate();
	BoardCursor_init(&currentEvent->cursor);
	*isLocked = false;
	*currentDisp = STATIONARY;
	Board_move(*board, 0, 0);
}

static void UI_quit_game(Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, const StartupSettings settings)
{
	free(settings.fileName);
	CoreBoard_free(coreBoard, settings.n);
	Board_free(board);
	StatusBar_free(bar);
	endwin();
	printf("\nGood bye ! :-)\n");
}

static void UI_restart_game(StartupSettings * const settings, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * const currentEvent, size_t * const currentPlayer, bool * const isLocked, size_t * const roundCounter, Displacement * const currentDisp)
{
	CoreBoard_free(coreBoard, settings->n);
	Board_free(board);
	StatusBar_free(bar);
	endwin();
	UI_init_game(settings, board, coreBoard, bar, currentEvent, currentPlayer, isLocked, roundCounter, currentDisp);
}

LIST_PUSH_FRONT(Coordinate, Coordinate)
LIST_EMPTY(Coordinate, Coordinate)

static bool UI_update_player(size_t * currentPlayer, Displacement * const disp, const NumberOfPlayer n, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, BoardCursor * const currentCursor, const bool currentLockState, ListCoordinate * const previousJumpPositions, const BoardCursor previousCursor, size_t * const roundCounter)
{
	bool isLocked;
	Coordinate what, previous;
	what.y = currentCursor->currentY;
	what.x = currentCursor->currentX;
	isLocked = currentLockState;
	if (*disp == JUMPED) {
		previous.y = previousCursor.savedY;
		previous.x = previousCursor.savedX;
		ListCoordinate_push_front(previousJumpPositions, previous);
	} if ((*disp == JUMPED) && CoreBoard_can_jump(coreBoard, *currentPlayer, what, previousJumpPositions)) {
		isLocked = true;
		BoardCursor_select(currentCursor);
		Board_select(board, currentCursor->savedY, currentCursor->savedX);
		*disp = STATIONARY;
	} else if ((*disp == WALKED) || (*disp == JUMPED)) {
		isLocked = false;
		*currentPlayer = (*currentPlayer + 1) % n;
		(*roundCounter)++;
		*disp = STATIONARY;
		ListCoordinate_empty(previousJumpPositions);
	}
	return isLocked;
}

static void UI_move_cursor(const BoardCursor previousCursor, const BoardCursor currentCursor, Board * const board, WINDOW * const bar)
{
	char msg[11];
	sprintf(msg, "MOVCUR %d,%d", currentCursor.currentY, currentCursor.currentX);
	Board_restore(board, previousCursor.currentY, previousCursor.currentX);
	Board_move(board, currentCursor.currentY, currentCursor.currentX);
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
}

static void UI_select_peice(BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
{
	char msg[11];
	Coordinate what;
	what.y = currentCursor->savedY;
	what.x = currentCursor->savedX;
	if (CoreBoard_is_owner(coreBoard, Players_symbol_for(currentPlayer), what)) {
		sprintf(msg, "SELECT %d,%d", currentCursor->savedY, currentCursor->savedX);
		Board_select(board, currentCursor->savedY, currentCursor->savedX);
	} else {
		sprintf(msg, "NOTOWNER");
		BoardCursor_unselect(currentCursor);
	}
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
}

static void UI_unselect_peice(BoardCursor * const currentCursor, Board * const board, WINDOW * const bar, const bool isLocked)
{
	char msg[11];
	if (! isLocked) {
		sprintf(msg, "UNSELE %d,%d", currentCursor->savedY, currentCursor->savedX);
		Board_unselect(board, currentCursor->savedY, currentCursor->savedX);
		BoardCursor_unselect(currentCursor);
	} else {
		sprintf(msg, "NOTALLOWED");
	}
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
}

static Displacement UI_move_piece(BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
{
	char msg[11];
	Displacement disp;
	Coordinate what, where;
	what.y = currentCursor->savedY;
	what.x = currentCursor->savedX;
	where.y = currentCursor->currentY;
	where.x = currentCursor->currentX;
	if ((disp = CoreBoard_move(coreBoard, currentPlayer, what, where)) != STATIONARY) {
		sprintf(msg, "MOVPIC %d,%d", currentCursor->currentY, currentCursor->currentX);
		Board_move_piece(board, currentCursor->savedY, currentCursor->savedX, currentCursor->currentY, currentCursor->currentX);
		Board_move(board, currentCursor->currentY, currentCursor->currentX);
		Board_unselect(board, currentCursor->savedY, currentCursor->savedX);
		BoardCursor_unselect(currentCursor);
	} else {
		sprintf(msg, "NOTALLOWED");
	}
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
	return disp;
}

static void UI_move_cursor_and_select(const BoardCursor previousCursor, BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
{
	char msg[11];
	Coordinate what;
	what.y = currentCursor->savedY;
	what.x = currentCursor->savedX;
	if (CoreBoard_is_owner(coreBoard, Players_symbol_for(currentPlayer), what)) {
		sprintf(msg, "SELECT %d,%d", currentCursor->savedY, currentCursor->savedX);
		Board_restore(board, previousCursor.currentY, previousCursor.currentX);
		Board_move(board, currentCursor->currentY, currentCursor->currentX);
		Board_select(board, currentCursor->savedY, currentCursor->savedX);
	} else {
		sprintf(msg, "NOTOWNER");
		currentCursor->currentY = previousCursor.currentY;
		currentCursor->currentX = previousCursor.currentX;
		BoardCursor_unselect(currentCursor);
	}
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
}

static Displacement UI_move_cursor_and_move_piece(const BoardCursor previousCursor, BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
{
	char msg[11];
	Displacement disp;
	Coordinate what, where;
	what.y = currentCursor->savedY;
	what.x = currentCursor->savedX;
	where.y = currentCursor->currentY;
	where.x = currentCursor->currentX;
	if ((disp = CoreBoard_move(coreBoard, currentPlayer, what, where)) != STATIONARY) {
		sprintf(msg, "MOVPIC %d,%d", currentCursor->savedY, currentCursor->savedX);
		Board_restore(board, previousCursor.currentY, previousCursor.currentX);
		Board_move_piece(board, currentCursor->savedY, currentCursor->savedX, currentCursor->currentY, currentCursor->currentX);
		Board_move(board, currentCursor->currentY, currentCursor->currentX);
		BoardCursor_unselect(currentCursor);
	} else {
		sprintf(msg, "NOTALLOWED");
		currentCursor->currentY = previousCursor.currentY;
		currentCursor->currentX = previousCursor.currentX;
	}
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
	return disp;
}


static void UI_check_screensize(const StartupSettings settings, int  * const defaultLines, int * const defaultCols, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * event, size_t * const currentPlayer)
{
	int lines, cols;
	getmaxyx(stdscr, lines, cols);
	if ((lines < *defaultLines) || (cols < *defaultCols)) {
		UI_quit_game(board, coreBoard, bar, settings);
		printf("\nDO NOT DOWNSIZE THE WINDOW !\n");
		exit(EXIT_FAILURE);
	}
	*defaultLines = lines;
	*defaultCols = cols;
	/*if (*defaultLines != lines || *defaultCols != cols) {
		//Board_check_screensize();
		//UI_restart_game(settings, board, coreBoard, bar, event, currentPlayer);
		//*defaultLines = lines;
		//*defaultCols = cols;
	}*/
}

static Option UI_detect_winner_and_loser(WINDOW * const bar, CoreBoard * const board, const size_t currentPlayer, const NumberOfPlayer n, const size_t roundCounter)
{
	Option option;
	char msg[11];
	option = OPTION_NONE;
	if (CoreBoard_did_player_win(board, currentPlayer)) {
		option = OPTION_RESTART;
		sprintf(msg, "PLR %c WON!", Players_symbol_for(currentPlayer));
		StatusBar_erase_msg(bar);
		StatusBar_display_10chr_msg(bar, msg);
		getch();
	} else {
		if (roundCounter == 30) {
			if (CoreBoard_is_any_piece_unmoved(board, n)) {
				option = OPTION_RESTART;
				sprintf(msg, "LOSER !", Players_symbol_for(currentPlayer));
				StatusBar_erase_msg(bar);
				StatusBar_display_10chr_msg(bar, msg);
				getch();
			}
		}
	}
	return option;
}

static void UI_skip_player(CoreBoard * const coreBoard, WINDOW * const bar, Displacement * const currentDisp, const size_t currentPlayer)
{
	char msg[11];
	if (CoreBoard_can_skip(coreBoard, currentPlayer)) {
		sprintf(msg, "PLAYR SKIP");
		*currentDisp = WALKED;
	} else {
		sprintf(msg, "CANNOTSKIP");
	}
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
}

static void UI_bot_move(Board * const board, WINDOW * const bar, const Coordinate what, const Coordinate where)
{
	char msg[11];
	sprintf(msg, "BOTMOV %d,%d", where.y, where.x);
	Board_move_piece(board, what.y, what.x, where.y, where.x);
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
}

static void UI_skip_bot(Displacement * const currentDisp, WINDOW * const bar)
{
	char msg[11];
	sprintf(msg, "BOTSKIPPED");
	*currentDisp = WALKED;
	StatusBar_erase_msg(bar);
	StatusBar_display_10chr_msg(bar, msg);
}

static void UI_who_plays(StartupSettings settings, const size_t currentPlayer, Displacement * const currentDisp, CoreBoard * const coreBoard, Board * const board, WINDOW * const bar, GameEvent * const currentEvent, GameEvent * const previousEvent)
{
	BotAction botAction;
	if (currentPlayer < settings.b) {
		botAction = Bot_move(coreBoard, currentPlayer, settings.difficulty);
		if (botAction.disp == STATIONARY) {
			UI_skip_bot(currentDisp, bar);
		} else {
			*currentDisp = botAction.disp;
			UI_bot_move(board, bar, botAction.what, botAction.where);
		}
		currentEvent->option = OPTION_NONE;
	} else {
		*previousEvent = *currentEvent;
		Event_get_event(currentEvent, board->whole, bar);
	}
}

LIST_MALLOC(Coordinate, Coordinate)

void UI_run(void)
{
	CoreBoard * coreBoard;
	Board * board;
	WINDOW * bar;
	StartupSettings settings;
	Displacement currentDisp;
	int defaultLines, defaultCols;
	size_t currentPlayer, roundCounter;
	GameEvent previousEvent, currentEvent;
	bool isLocked;
	ListCoordinate * previousJumpPositions;
	previousJumpPositions = ListCoordinate_malloc();
	settings = StartupMenu_run();
	UI_init_game(&settings, &board, &coreBoard, &bar, &currentEvent, &currentPlayer, &isLocked, &roundCounter, &currentDisp);
	getmaxyx(stdscr, defaultLines, defaultCols);
	do {
		StatusBar_set_player(bar, Players_symbol_for(currentPlayer));
		UI_who_plays(settings, currentPlayer, &currentDisp, coreBoard, board, bar, &currentEvent, &previousEvent);
		switch (currentEvent.option) {
			case OPTION_MOVE_CURSOR:
				UI_move_cursor(previousEvent.cursor, currentEvent.cursor, board, bar);
				break;
			case OPTION_SELECT:
				UI_select_peice(&currentEvent.cursor, board, coreBoard, bar, currentPlayer);
				break;
			case OPTION_UNSELECT:
				UI_unselect_peice(&currentEvent.cursor, board, bar, isLocked);
				break;
			case OPTION_MOVE_PIECE:
				currentDisp = UI_move_piece(&currentEvent.cursor, board, coreBoard, bar, currentPlayer);
				break;
			case OPTION_MOVE_CURSOR_AND_SELECT:
				UI_move_cursor_and_select(previousEvent.cursor, &currentEvent.cursor, board, coreBoard, bar, currentPlayer);
				break;
			case OPTION_MOVE_CURSOR_AND_PIECE:
				currentDisp = UI_move_cursor_and_move_piece(previousEvent.cursor, &currentEvent.cursor, board, coreBoard, bar, currentPlayer);
				break;
			case OPTION_RESTART:
				UI_restart_game(&settings, &board, &coreBoard, &bar, &currentEvent, &currentPlayer, &isLocked, &roundCounter, &currentDisp);
				break;
			case OPTION_HELP:
				Option_help(bar, board->whole);
				break;
			case OPTION_SAVE:
				if (isLocked) {
					StatusBar_erase_msg(bar);
					StatusBar_display_10chr_msg(bar, "FINISHMOV");
				} else {
					Option_save(bar, board->whole, coreBoard, currentPlayer, roundCounter, settings.n, settings.b, settings.difficulty);
				}
				break;
			case OPTION_PASS:
				UI_skip_player(coreBoard, bar, &currentDisp, currentPlayer);
				break;
			default:
				UI_check_screensize(settings, &defaultLines, &defaultCols, &board, &coreBoard, &bar, &currentEvent, &currentPlayer);
				break;
		}
		if (UI_detect_winner_and_loser(bar, coreBoard, currentPlayer, settings.n, roundCounter) == OPTION_RESTART) {
			UI_restart_game(&settings, &board, &coreBoard, &bar, &currentEvent, &currentPlayer, &isLocked, &roundCounter, &currentDisp);
		}
		isLocked = UI_update_player(&currentPlayer, &currentDisp, settings.n, board, coreBoard, bar, &currentEvent.cursor, isLocked, previousJumpPositions, previousEvent.cursor, &roundCounter);
	} while (currentEvent.option != OPTION_QUIT);
	UI_quit_game(&board, &coreBoard, &bar, settings);
}
