/**
 * \file interface.h
 * \brief This file contains headers of routines related to the interface of the game.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 10:31 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __INTERFACE_H__
	#define __INTERFACE_H__
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <ncurses.h>
	
	#include "event.h"
	#include "board.h"
	#include "status_bar.h"
	#include "start_menu.h"
	#include "option.h"

	#include "../Core/coreboard.h"
	#include "../Core/minimax.h"
	#include "../Lib/List/list.h"
	#include "../Core/save.h"


	/**
	 * \fn static void UI_init_game(StartupSettings * const settings, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * const currentEvent, size_t * const currentPlayer, bool * const isLocked, size_t * const roundCounter, Displacement * const currentDisp)
	 * \brief Initializes the UI and Core structures for the game.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:29 AM
	 */
	static void UI_init_game(StartupSettings * const settings, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * const currentEvent, size_t * const currentPlayer, bool * const isLocked, size_t * const roundCounter, Displacement * const currentDisp);

	/**
	 * \fn static void UI_quit_game(Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, const StartupSettings settings)
	 * \brief Quits the game and frees all UI objects and Core objects.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:37 AM
	 */
	static void UI_quit_game(Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, const StartupSettings settings);

	/**
	 * \fn static void UI_restart_game(StartupSettings * const settings, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * const currentEvent, size_t * const currentPlayer, bool * const isLocked, size_t * const roundCounter, Displacement * const currentDisp)
	 * \brief Restarts the game.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:38 AM
	 */
	static void UI_restart_game(StartupSettings * const settings, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * const currentEvent, size_t * const currentPlayer, bool * const isLocked, size_t * const roundCounter, Displacement * const currentDisp);

	/**
	 * \fn static bool UI_update_player(size_t * currentPlayer, Displacement * const disp, const NumberOfPlayer n, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, BoardCursor * const currentCursor, const bool currentLockState, ListCoordinate * const previousJumpPositions, const BoardCursor previousCursor, size_t * const roundCounter)
	 * \brief Changes current player.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:38 AM
	 */
	
	static bool UI_update_player(size_t * currentPlayer, Displacement * const disp, const NumberOfPlayer n, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, BoardCursor * const currentCursor, const bool currentLockState, ListCoordinate * const previousJumpPositions, const BoardCursor previousCursor, size_t * const roundCounter);

	/**
	 * \fn static void UI_move_cursor(const BoardCursor previousCursor, const BoardCursor currentCursor, Board * const board, WINDOW * const bar)
	 * \brief Moves cursor graphically and inside the \a CoreBoard structure.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:39 AM
	 */
	static void UI_move_cursor(const BoardCursor previousCursor, const BoardCursor currentCursor, Board * const board, WINDOW * const bar);

	/**
	 * \fn static void UI_select_peice(BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
	 * \brief Selects a piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:39 AM
	 */
	static void UI_select_peice(BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer);

	/**
	 * \fn static void UI_unselect_peice(BoardCursor * const currentCursor, Board * const board, WINDOW * const bar, const bool isLocked)
	 * \brief Unselects a piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:40 AM
	 */
	static void UI_unselect_peice(BoardCursor * const currentCursor, Board * const board, WINDOW * const bar, const bool isLocked);

	/**
	 * \fn static Displacement UI_move_piece(BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
	 * \brief Moves a player piece.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:40 AM
	 */
	static Displacement UI_move_piece(BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer);

	/**
	 * \fn static void UI_move_cursor_and_select(const BoardCursor previousCursor, BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
	 * \brief Moves cursor and selects the piece under it.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:40 AM
	 */
	static void UI_move_cursor_and_select(const BoardCursor previousCursor, BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer);

	/**
	 * \fn static Displacement UI_move_cursor_and_move_piece(const BoardCursor previousCursor, BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer)
	 * \brief Moves cursor and moves the pieces at the new cursor location.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:40 AM
	 */
	static Displacement UI_move_cursor_and_move_piece(const BoardCursor previousCursor, BoardCursor * const currentCursor, Board * const board, CoreBoard * const coreBoard, WINDOW * const bar, const size_t currentPlayer);

	/**
	 * \fn static void UI_check_screensize(const StartupSettings settings, int  * const defaultLines, int * const defaultCols, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * event, size_t * const currentPlayer)
	 * \brief Checks screen size and exit the game if user downsized the terminal window.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:41 AM
	 */
	static void UI_check_screensize(const StartupSettings settings, int  * const defaultLines, int * const defaultCols, Board * * const board, CoreBoard * * const coreBoard, WINDOW * * const bar, GameEvent * event, size_t * const currentPlayer);

	/**
	 * \fn static Option UI_detect_winner_and_loser(WINDOW * const bar, CoreBoard * const board, const size_t currentPlayer, const NumberOfPlayer n, const size_t roundCounter)
	 * \brief Tests if there is a winner.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:41 AM
	 */
	static Option UI_detect_winner_and_loser(WINDOW * const bar, CoreBoard * const board, const size_t currentPlayer, const NumberOfPlayer n, const size_t roundCounter);

	/**
	 * \fn static void UI_skip_player(CoreBoard * const coreBoard, WINDOW * const bar, Displacement * const currentDisp, const size_t currentPlayer)
	 * \brief Skips current player turn.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:41 AM
	 */
	static void UI_skip_player(CoreBoard * const coreBoard, WINDOW * const bar, Displacement * const currentDisp, const size_t currentPlayer);

	/**
	 * \fn static void UI_bot_move(Board * const board, WINDOW * const bar, const Coordinate what, const Coordinate where)
	 * \brief Moves bot pieces.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:41 AM
	 */
	static void UI_bot_move(Board * const board, WINDOW * const bar, const Coordinate what, const Coordinate where);

	/**
	 * \fn static void UI_skip_bot(Displacement * const currentDisp, WINDOW * const bar)
	 * \brief Skips current bot turn.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:41 AM
	 */
	static void UI_skip_bot(Displacement * const currentDisp, WINDOW * const bar);

	/**
	 * \fn static void UI_who_plays(StartupSettings settings, const size_t currentPlayer, Displacement * const currentDisp, CoreBoard * const coreBoard, Board * const board, WINDOW * const bar, GameEvent * const currentEvent, GameEvent * const previousEvent)
	 * \brief Determines which one between bots and players have to play.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:40 AM
	 */
	static void UI_who_plays(StartupSettings settings, const size_t currentPlayer, Displacement * const currentDisp, CoreBoard * const coreBoard, Board * const board, WINDOW * const bar, GameEvent * const currentEvent, GameEvent * const previousEvent);

	/**
	 * \fn void UI_run(void)
	 * \brief Runs the entire UI.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 10:40 AM
	 */
	void UI_run(void);
#endif
