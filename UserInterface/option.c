/**
 * \file option.c
 * \brief This files contains routines related to status bar options.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 10:09 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "option.h"

void Option_help(WINDOW * const bar, WINDOW * const board)
{
	def_prog_mode();
	endwin();
	clear_screen();
	puts("HELP MENU:\n");
	puts("- Top left corner (named player) indicates who's turn is it to play.");
	puts("- At any given time you can move the cursor, select a piece, or move a piece once it has been selected. (see below for instructions on how to perform these 3 different operations)");
	puts("\nMOUSE OPTIONS:\n");
	puts("- SIMPLE LEFT CLICK --> moves cursor at the click position");
	puts("- DOUBLE LEFT CLICK --> selects a piece or moves a piece at the double click position");
	puts("- SIMPLE RIGHT CLICK --> unselects if a piece is currently selected or else do nothing");
	puts("\nKEYBOARD OPTIONS:\n");
	puts("- ARROWS --> moves the cursor left, right, up, or down depending on the arrow key pressed.");
	puts("- ENTER --> selects, or moves the piece at the current cursor position.");
	puts("- 'p' --> passes turn to the next player if no moves available.");
	puts("- 'u' --> unselects if a piece is currently selected or else do nothing.");
	puts("- 's' --> saves the game (not yet implemented).");
	puts("- 'q' --> quits the game.");
	puts("\npress any key to go back to the game...");
	getchar();
	reset_prog_mode();
	refresh();
	wrefresh(bar);
	wrefresh(board);
}

void Option_save(WINDOW * const bar, WINDOW * const board, const CoreBoard * const coreBoard, const size_t currentPlayerIndex, const size_t roundCounter, const NumberOfPlayer numberOfPlayer, const short botNumber, const short difficulty)
{
	def_prog_mode();
	endwin();
	clear_screen();
	save_board_state(coreBoard, currentPlayerIndex, roundCounter, numberOfPlayer, botNumber, difficulty);
	printf("\npress any key to go back to the game...");
	getchar();
	reset_prog_mode();
	refresh();
	wrefresh(bar);
	wrefresh(board);
}
