/**
 * \file start_menu.c
 * \brief This file contains routines related to the startup menu.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 09:54 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "start_menu.h"

static short StartupMenu_get_int_in_range(char * const msg, const short a, const short b)
{
	short n;
	do {
		n = secure_inputint(msg);
	} while ((n < a) || (n > b));
	return n;
}

static short StartupMenu_get_theme(void)
{
	short color;
	puts("\nTheme list :");
	puts("\
	1\tRED\n\
	2\tGREEN\n\
	3\tYELLOW\n\
	4\tBLUE\n\
	5\tMAGENTA\n\
	6\tCYAN\
	");
	do {
		color = secure_inputint("\nEnter a color theme number ? [1-6] : ");
	} while ((color < 1) || (color > 6));
	return color;
}

static char * StartupMenu_get_save_path(void)
{
	char * name, * path;
	name = path = NULL;
	do {
		free(path);
		free(name);
		name = secure_inputstr("\nEnter the name file ? : ");
		if ((path = malloc((strlen(name) + 6) * sizeof(char))) == NULL) {
			fprintf(stderr, "error: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		sprintf(path, "Data/%s", name);
	} while (access(path, R_OK) == -1);
	free(name);
	return path;
}

StartupSettings StartupMenu_run(void)
{
	StartupSettings startupMenu;
	char * choice;
	bool isValid;
	clear_screen();
	printf("Welcome to the startup menu:\n- The Text User Interface will be initialize shortly after you answered few questions.\n- Please be sure that you terminal window is at least %dx%d char squared.\n", BOARD_SCREEN_HEIGHT+1, BOARD_SCREEN_WIDTH);
	startupMenu.fileName = NULL;
	do {
		choice = secure_inputstr("\nDo you want to load a save ? Y/n : ");
		isValid = false;
		if ((strcmp(choice, "y") == 0) || (strcmp(choice, "Y") == 0)) {
			startupMenu.fileName = StartupMenu_get_save_path();
			isValid = true;
		} else if ((strcmp(choice, "n") == 0) || (strcmp(choice, "N") == 0)) {
			startupMenu.n = (NumberOfPlayer)StartupMenu_get_int_in_range(" \nHow many players ? [2-4] : ", 2, 4);
			printf("\nHow many bots ? [0 to %d]:", startupMenu.n - 1);
			startupMenu.b = StartupMenu_get_int_in_range(" ", 0, startupMenu.n - 1);
			startupMenu.difficulty = 1;
			if (startupMenu.b != 0) {
				startupMenu.difficulty = StartupMenu_get_int_in_range("\nWhat difficulty level ? [1-6] (1 by default): ", 1, 6);
			}
			isValid = true;
		}
		free(choice);
	} while (! isValid);
	startupMenu.theme = StartupMenu_get_theme();
	return startupMenu;
}
