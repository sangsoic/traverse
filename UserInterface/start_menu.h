/**
 * \file start_menu.h
 * \brief This file contains header of routines related to the startup menu.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 09:54 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __START_MENU_H__
	#define __START_MENU_H__
	
	#include <stdlib.h>
	#include <stdio.h>
	#include <ncurses.h>
	#include <string.h>
	#include <unistd.h>
	#include <stdbool.h>

	#include "../Core/terminal_io.h"

	#define BOARD_SQUARE_HEIGHT 10
	#define BOARD_SQUARE_WIDTH 10

	#define SQUARE_SCREEN_HEIGHT 4
	#define SQUARE_SCREEN_WIDTH 6

	#define BOARD_SCREEN_HEIGHT BOARD_SQUARE_HEIGHT * SQUARE_SCREEN_HEIGHT
	#define BOARD_SCREEN_WIDTH BOARD_SQUARE_WIDTH * SQUARE_SCREEN_WIDTH

	typedef enum {
		TWO_PLAYER = 2,
		THREE_PLAYER,
		FOUR_PLAYER
	} NumberOfPlayer;

	typedef struct {
		NumberOfPlayer n;
		short b;
		short difficulty;
		short theme;
		char * fileName;
	} StartupSettings;

	/**
	 * \fn static short StartupMenu_get_int_in_range(char * const msg, const short a, const short b)
	 * \brief Gets a short integer from stdin between a given range and display a message.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:56 AM
	 * \param msg Message to display.
	 * \param a Minimum value.
	 * \param b Maximum value.
	 * \return Short integers from stdin
	 */
	static short StartupMenu_get_int_in_range(char * const msg, const short a, const short b);

	/**
	 * \fn static short StartupMenu_get_theme(void)
	 * \brief Asks user about the color theme.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:56 AM
	 * \return Color theme value.
	 */
	static short StartupMenu_get_theme(void);

	/**
	 * \fn static char * StartupMenu_get_save_path(void)
	 * \brief Asks user the name of the save file.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-28 Thu 02:32 AM
	 * \return Path of the save file.
	 */
	static char * StartupMenu_get_save_path(void);

	/**
	 * \fn StartupSettings StartupMenu_run(void)
	 * \brief Runs startup CLI menu.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:56 AM
	 * \return Startup settings.
	 */
	StartupSettings StartupMenu_run(void);

#endif
