/**
 * \file status_bar.h
 * \brief This file contains headers of routine specific to the status bar.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 09:37 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "status_bar.h"

static WINDOW * StatusBar_allocate(void)
{
	WINDOW * bar;
	int lines, cols;
	getmaxyx(stdscr, lines, cols);
	bar = newwin(1, cols, 0, 0);
	if (bar == NULL) {
		endwin();
		fprintf(stderr, "error ncurses : cannot allocate a window.\n");
		exit(EXIT_FAILURE);
	}
	return bar;
}

static void StatusBar_init(WINDOW * const bar)
{
	int lines, cols;
	refresh();
	getmaxyx(bar, lines, cols);
	wattron(bar, COLOR_PAIR(3) | A_UNDERLINE | A_BOLD);
	mvwaddstr(bar, 0, 0, "PLAYER:N |");
	waddstr(bar, " HELP:H  | SAVE:S  | QUIT:Q  |RESTART:R|");
	whline(bar, ' ', cols - 50);
	if (wrefresh(bar) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

WINDOW * StatusBar_generate(void)
{
	WINDOW * bar;
	bar = StatusBar_allocate();
	init_pair(3, COLOR_BLACK, COLOR_WHITE);
	StatusBar_init(bar);
	return bar;
}

void StatusBar_set_player(WINDOW * const bar, const Players symbol)
{
	mvwaddch(bar, 0, 7, symbol);
	if (wrefresh(bar) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void StatusBar_erase_msg(WINDOW * const bar)
{
	mvwhline(bar, 0, 50, ' ', 10);
	if (wrefresh(bar) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void StatusBar_display_10chr_msg(WINDOW * const bar, const char * const msg)
{
	StatusBar_erase_msg(bar);
	mvwaddnstr(bar, 0, 50, msg, 10);
	if (wrefresh(bar) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

void StatusBar_free(WINDOW * * const bar)
{
	WINDOW * tmp;
	tmp = *bar;
	if (delwin(tmp) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot delete window.\n");
		exit(EXIT_FAILURE);
	}
	*bar = NULL;
}
