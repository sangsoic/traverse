/**
 * \file status_bar.h
 * \brief This file contains headers of routine specific to the status bar.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-03 Mon 09:37 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __STATUS_BAR_H__
	#define __STATUS_BAR_H__

	#include <errno.h>
	#include <string.h>
	#include <ncurses.h>
	#include <stdio.h>
	#include <stdlib.h>

	#include "board.h"

	/**
	 * \fn static WINDOW * StatusBar_allocate(void)
	 * \brief Allocates status bar.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:40 AM
	 * \return Newly allocated status bar.
	 */
	static WINDOW * StatusBar_allocate(void);

	/**
	 * \fn static void StatusBar_init(WINDOW * const bar)
	 * \brief Initializes a status bar.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:41 AM
	 * \param bar A given status bar.
	 */
	static void StatusBar_init(WINDOW * const bar);


	/**
	 * \fn WINDOW * StatusBar_generate(void)
	 * \brief Generates a status bar.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:44 AM
	 * \return Newly allocated status bar.
	 */
	WINDOW * StatusBar_generate(void);

	/**
	 * \fn void StatusBar_set_player(WINDOW * const bar, const Players symbol)
	 * \brief Changes status bar player symbol.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:46 AM
	 * \param bar A given status bar.
	 * \param symbol Player symbol.
	 */
	void StatusBar_set_player(WINDOW * const bar, const Players symbol);

	/**
	 * \fn void StatusBar_display_10chr_msg(WINDOW * const bar, const char * const msg)
	 * \brief Displays a short 10 characters message on a status bar.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:46 AM
	 * \param bar A given status bar.
	 * \param msg short 10 characters string.
	 */
	void StatusBar_display_10chr_msg(WINDOW * const bar, const char * const msg);

	/**
	 * \fn void StatusBar_erase_msg(WINDOW * const bar)
	 * \brief Erases the message area of a status bar.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:46 AM
	 * \param bar A given status bar.
	 */
	void StatusBar_erase_msg(WINDOW * const bar);

	/**
	 * \fn void StatusBar_free(WINDOW * * const bar)
	 * \brief Frees a status bar.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 09:46 AM
	 * \param bar Address of a pointer on a status bar object.
	 */
	void StatusBar_free(WINDOW * * const bar);

#endif
